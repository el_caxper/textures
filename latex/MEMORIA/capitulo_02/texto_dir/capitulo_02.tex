%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%			Capitulo 2					              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter[Tecnología de texturas utilizadas.]{Tecnología de texturas utilizadas} \label{cap2}

En este capítulo se muestran algunos de los métodos de análisis de texturas usado en la actualidad. Para ello se dividen los métodos de análisis de texturas en distintas categorías según el tipo de descriptores usados para el análisis. 
El análisis de textura es un problema básico en el procesamiento de imágenes y la visión por computador. Es un problema clave en muchas áreas, como el reconocimiento de objetos, en la teledetección (detección remota), la consulta de imágenes mediante ejemplo, etc.

\newpage

\section{Introducción}
 El análisis de textura ha sido un tema de investigación activo durante más de tres décadas. Numerosos métodos han sido propuestos en la literatura. Existen varias disciplinas y aplicaciones en las que la textura juega un papel importante. La textura juega un papel crítico en la inspección de superficies que se producen en las distintas etapas en todos los tipos de fabricación. Por ejemplo, en la inspección de semiconductores, la textura de la superficie es un factor importante que se utiliza para decidir la integridad de un dispositivo fabricado \citep{book:963433}. Sin embargo, la mayoría de los métodos de análisis de textura existentes hacen la suposición explícita o implícita de que las imágenes de textura se adquieren desde el mismo punto de vista (por ejemplo, la misma escala y orientación). Esto da una limitación a estos métodos. En muchas aplicaciones prácticas es muy difícil o imposible garantizar que las imágenes capturadas tengan las mismas traslaciones, rotaciones o escalas entre ellas. El análisis de textura debe ser idealmente invariante para los puntos de vista. El análisis de textura invariante es, por lo tanto, altamente deseable desde el punto de vista práctico y teórico \citep{book:144340}.

Se ha prestado cada vez más atención al análisis de textura invariable. Se ha trabajado mucho en este tema. Sin embargo, pocos trabajos existentes discuten este tema a pesar de su importancia creciente (por ejemplo, en la consulta de imágenes mediante ejemplo). 

Los métodos de análisis de textura existentes se dividen en general en dos categorías: métodos estadísticos y métodos estructurales. Pero en la literatura reciente, muchos métodos basados en modelos~\citep{chellappa1993model} se han empleado en análisis de texturas incluyendo el modelo autorregresivo~\citep{vaishali20142}, modelos wavelet~\citep{kingsbury1998dual}, etc. Estos modelos proporcionan herramientas más potentes para el análisis de texturas invariantes. Como tienen muchas propiedades similares, estos métodos se clasifican en una categoría independiente para presentar una clasificación más clara. Por lo tanto, los métodos descritos a continuación se dividen en tres categorías: métodos estadísticos, métodos basados en modelos y métodos estructurales. 

\section{Métodos estadísticos}

Una de las cualidades que más define la textura es la distribución espacial de los valores grises. El uso de características estadísticas es, por lo tanto, uno de los primeros métodos propuestos en la literatura de visión artificial. A continuación, se define $\{I(x,y), 0\le x \le N -1, 0\le y \le N -1\} $ para denotar una imagen de $N\times N$ con niveles grises. Se ha propuesto una gran cantidad de características de textura. La relación entre las diversas medidas de textura estadística y la imagen de entrada se resume en la Figura \ref{Figura2_01} \citep{computer_analysis}.


\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.6\linewidth]{capitulo_02/imagenes_dir/diagram1.pdf}
	\caption{Interrelación entre las diversas estadísticas de segundo orden y la imagen de entrada\citep{computer_analysis}}
	\label{Figura2_01}
\end{figure}

Uno de los enfoques más simples para describir la textura es usar momentos estadísticos del histograma de intensidad de una imagen o región \citep{gonzalez2002digital}. El uso de solo histogramas en el cálculo dará como resultado medidas de textura que solo transportarán información sobre la distribución de intensidades, pero no sobre la posición relativa de los píxeles entre sí en esa textura. El uso de un enfoque estadístico, como la matriz de coocurrencia, ayudará a proporcionar información valiosa sobre la posición relativa de los píxeles vecinos en una imagen.

Dada una imagen $I$ de tamaño $N\times N$, la matriz de coocurrencia, $P$ se puede definir como:

\begin{equation}\label{coocurrence}
P(i,j)= \sum_{x=1}^{N}\sum_{y=1}^{N} \begin{cases}
1, ~~ si ~~ I(x,y)=i ~~y~~ I(x+\Delta_x,y+\Delta_y)=j\\
0, ~~ en~otro~caso
\end{cases}
\end{equation}

Aquí, el desplazamiento $(\Delta_x,\Delta_y)$ especifica la distancia entre el píxel de interés y su vecino. Hay que tener en cuenta que la parametrización de desplazamiento $(\Delta_x,\Delta_y)$ hace que la matriz de co-ocurrencia sea sensible a la rotación. Este valor de desplazamiento se usa más adelante como parámetros en las distintas partes del sistema. %Elegir un vector de desplazamiento, de modo que la rotación de la imagen no sea igual a 180 grados, dará como resultado una matriz de co-ocurrencia diferente para la misma imagen (girada).

\subsection{Características de Haralick} \label{Haralick_features}

En 1973, Haralick \citep{haralick1973textural} introdujo 14 características estadísticas. Estas características se generan al calcular las características para cada una de las matrices de coocurrencia obtenidas al usar las direcciones $0^\circ$, $45^\circ$, $90^\circ$ y $135^\circ$, y luego promediar estos cuatro valores. El símbolo $\Delta$, que representa el parámetro de distancia, se puede seleccionar como uno o más alto. En general, el valor $\Delta$ se establece en 1 como parámetro de distancia. Se utiliza un vector de estas 14 características estadísticas para caracterizar los contenidos de la matriz de coocurrencia. 

La notación usada para definir las características es la siguiente:

$p(i,j)$ es el elemento $(i,j)$ de matriz de coocurrencia normalizada. 
$N_g$ denota la dimensión de la matriz de coocurrencia (número de niveles de grises).
$p_x(i)$ y $p_y(j)$ son las probabilidades marginales que se definen como:
\begin{equation}
p_x(i) = \sum_{j=1}^{N_g}p(i,j),~~ p_y(j) = \sum_{j=1}^{N_g}p(i,j)
\end{equation}

$\mu$ es la media de $\mu_x$ y $\mu_y$ donde:
\begin{equation}
\mu_x=\sum_{i=1}^{N_g}ip_x(i),~~ \mu_y=\sum_{j=1}^{N_g}jp_y(j)
\end{equation}

$\sigma_x$ y $\sigma_y$ con las desviaciones estándar donde:
\begin{equation}
\sigma_x =  \sqrt{\sum_{i=1}^{N_g} p_x(i)(i-\mu_x)^2}
\end{equation}
\begin{equation}
\sigma_y =  \sqrt{\sum_{j=1}^{N_g} p_y(j)(j-\mu_y)^2}
\end{equation}

\begin{equation}
\sigma = \sum_i \sum_j (i-\mu)^2p(i,j)
\end{equation}

Estas características se pueden calcular usando las siguientes ecuaciones:

\subsubsection{Energía}
La energía se deriva del segundo momento angular (ASM). El ASM mide la uniformidad local de los niveles de gris. Cuando los píxeles son muy similares, el valor de ASM será grande.
\begin{equation} \label{ASM}
ASM = \sum_i\sum_j \{p(i,j)\}^2
\end{equation}
\begin{equation}
f_1 = \sqrt{ASM}
\end{equation}

\subsubsection{Contraste} 
Contraste (Momento 2 o desviación estándar) es una medida de intensidad o variaciones de nivel de gris entre el píxel de referencia y su vecino. El gran contraste refleja grandes diferencias de intensidad en GLCM:
\begin{equation} \label{Contrast}
f_2 = \sum_{n=0}^{N_g - 1} n^2 {\sum_{i=1}^{N_g} \sum_{j=1,|i-j|=n}^{N_g} p(i,j)} 
\end{equation}

\subsubsection{Correlación}
Muestra la dependencia lineal de los niveles de grises en la matriz de coocurrencia:
\begin{equation} \label{Correlation}
f_3 = \frac{\sum_i \sum_j (i-\mu_x)(j-\mu_y) p(i,j)}{\sigma_x\sigma_y}
\end{equation}

\subsubsection{Varianza}
\begin{equation} \label{InverseDifferentMoment}
f_4 = \sum_i \sum_j (i-\mu)^2 p(i,j)
\end{equation}

\subsubsection{Homogeneidad}
La homogeneidad mide qué tan cerca está la distribución de elementos en la GLCM a la diagonal de la GLCM. A medida que aumenta la homogeneidad, el contraste, típicamente, disminuye:
\begin{equation} \label{SumOfSquareVariances}
f_5 = \sum_i \sum_j {\frac{p(i,j)}{1 + (i-j)^2}}
\end{equation}

\subsubsection{Suma media}
\begin{equation} \label{SumAverage}
f_6 = \sum_{i=2}^{2N_g}ip_{x+y}(i)
\end{equation}
\subsubsection{Suma de varianza}
\begin{equation} \label{SumVariance}
f_7 = \sum_{i=2}^{2N_g}(i-f_8)^2p_{x+y}(i)
\end{equation}
\subsubsection{Suma de entropía}
\begin{equation}\label{SumEntropy}
f_8 = -\sum_{i=2}^{2N_g}p_{x+y}(i)\log\{p_{x+y}(i)\}
\end{equation}
\subsubsection{Entropía}
La entropía es la aleatoriedad o el grado de desorden presente en la imagen. El valor de la entropía es el más grande cuando todos los elementos de la matriz de coincidencia son iguales y pequeños cuando los elementos son desiguales:
\begin{equation}\label{Entropy}
f_9 = -\sum_i \sum_j p(i,j) \log_2(p(i,j)) \ o \ 0 \ si \ p(i,j)=0.
\end{equation}
\subsubsection{Diferencia de varianza}
\begin{equation}\label{DifferenceVariance}
f_{10} = \sum_{i=0}^{N_g-1} i^2 p_{x-y}(i)
\end{equation}
\subsubsection{Diferencia de entropía}
\begin{equation}\label{DifferenceEntropy}
f_{11} = -\sum_{i=0}^{N_g-1}p_{x-y}(i)\log\{p_{x-y}(i)\}
\end{equation}

\subsubsection{Medidas de información de correlación ($f_{12}$ y $f_{13}$)}
\begin{equation}\label{MessureCorrelation1}
f_{12} = \frac{HXY- HXY1}{\max\{HX,HY\}}
\end{equation}
\begin{equation}\label{MessureCorrelation2}
f_{13} = \sqrt{1-\exp(-2(HXY2-HXY))}
\end{equation}

\begin{equation}
HXY = -\sum_i \sum_j p(i,j) \log(p(i,j)) 
\end{equation}

Donde $HX$ y $HY$ son las entropías de $p_x$ y $p_y$ y:
\begin{equation}
HXY1 = -\sum_i \sum_j p(i,j) \log(p_x(i)p_y(j)) 
\end{equation}
\begin{equation}
HXY2 = -\sum_i \sum_j p_x(i)p_y(j) \log(p_x(i)p_y(j)) 
\end{equation}


\subsubsection{Coeficiente de correlación máximo}
\begin{equation}\label{MaximalCorrelationCoefficient}
f_{14} = (\textrm{Segundo mayor autovalor de } Q)^{1/2}
\end{equation}
Donde:
\begin{equation}
Q = \sum_k\frac{p(i,k)p(j,k)}{p_x(i)p_y(j)}
\end{equation}



\subsection{ Local Binary Pattern (LBP)}


Local  Binary  Pattern (LBP) \citep{ojala1994performance}  es  un  operador  de  textura  simple  y  eficiente  que 	etiqueta cada píxel de la imagen analizando su vecindario, estudiando si el nivel de gris de cada píxel supera un determinado umbral y codificando dicha comparación mediante un número binario. Debido a  su  bajo  coste  computacional  y  al  gran  poder  discriminativo  que  ha  demostrado  tener,  LBP  se  ha convertido  en  los  últimos  años  en  una  de  las  soluciones  más  utilizadas  en  numerosas  aplicaciones relacionadas con textura. La característica más importante de LBP en aplicaciones reales es la robustez que  ofrece  frente  a  variaciones  en  la  intensidad  del  nivel  de  gris,  causado,  entre  otras  muchas  cosas, por diferencias en la iluminación.

Para calcular LBP sobre una imagen en escala de grises se utiliza la ecuación \ref{eqn_lbp}.

\begin{equation} \label{eqn_lbp}
LBP_{P,R}= \sum_{p=0}^{P-1}s(g_p-g_c)2^p, ~~ s(x) =\begin{cases}
1, ~~\textrm{si}~~x\ge 0\\
0, ~~\textrm{si}~~x< 0
\end{cases}
\end{equation}

Donde P	es el número de vecinos que se van a considerar, R es el tamaño del vecindario y, $g_c$ y $g_p$ son los valores de gris del píxel central y cada uno de los $p$ píxeles del vecindario respectivamente.

En  2002, se propone una  mejora  del  método\citep{ojala2002multiresolution},  incorporando  vecindarios	circulares, donde los vecinos se ubicaban igualmente espaciados, realizando una interpolación bilineal en caso de que éstos no coincidieran con el centro de un píxel concreto de la imagen.

\section{Métodos estructurales}

En los métodos estructurales, consideran a la textura como una composición de elementos primitivos (características) bien definidos y distribuidos con algún patrón de repetición. En la literatura estos métodos de análisis se conocen como descripción de textura sintáctica, debido a que utilizan gramáticas que definen ciertas reglas de las primitivas. Algunos de estos métodos encontrados en \citep{sonka2014image} son: gramáticas de cadenas de formas, gramáticas de grafos y agrupamiento de primitivas en texturas jerárquicas.

Gramáticas de cadenas, son gramáticas simples que pueden ser usadas para descripción de textura. Generan textura empezando con un símbolo inicial, seguidos por la aplicación de reglas de transformación de formas. Mientras que las gramáticas de grafos construyen un grafo plano de diseño de textura primitivo, para lo cual se deben conocer las clases primitivas y sus relaciones espaciales permitiendo construir una estructura de grafos de textura, para su solución se aplican métodos de reconocimiento de grafos\citep{palomino2010procesamiento}. En el caso de agrupamiento de primitivas son ejecutados con primitivas de textura de bajo nivel que forman algún patrón específico el cual puede ser considerado como una primitiva en un nivel de descripción alto. 

\section{Métodos basados en modelos}

Estos métodos se basan en la construcción de un modelo, cuyos parámetros estimados describirían las cualidades de la textura. En esta sección consideraremos los campos aleatorios de Markov así como los fractales.

\subsection{Campos aleatorios de Markov}

Los métodos que utilizan los campos aleatorios de Markov gaussianos (Gaussian Marvov Random Fields) caracterizan la relación estadística entre un píxel y sus vecinos \citep{chellappa1985classification}. El modelo estocástico resultante consta de un número de parámetros igual al tamaño de la máscara de la vecindad. Los parámetros se pueden estimar mediante un algoritmo de mínimos cuadrados sobre cada posición de la mascara en la imagen. Una máscara que es bastante habitual es una máscara simétrica, como la que se muestra en la figura siguiente (es la que hemos empleado en los experimentos).


\subsection{Fractales}

El análisis de texturas basándose en fractales fue introducido por Pentland \citep{pentland1984fractal}, donde se mostró la correlación existente entre la dimensión fractal de una textura y su ``tosquedad'' (coarseness). La descripción fractal de texturas se basa en gran medida, en la determinación de la dimensión fractal. La propiedad de auto-similaridad implica que la dimensión fractal de una imagen es independiente de la escala.


