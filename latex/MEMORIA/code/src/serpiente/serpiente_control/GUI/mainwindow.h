#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "gusano.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    int invertido;
    Gusano gusanoVer;
    Gusano gusanoHor;
    int wi;

    int Ah,Av,Dfv,Dfh,Ov,Oh,Foh,Fov;
    unsigned int Th,Tv;

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btn_rodar_clicked();

    void on_btn_rotar_clicked();

    void on_btn_lateral_clicked();

    void on_btn_gusano_clicked();

    void on_btn_conectar_clicked();

    void on_btn_invertir_clicked();

    void killLoop();

    void on_btn_salir_clicked();

    void on_btn_enviar_clicked();

private:
    Ui::MainWindow *ui;
    bool killLoopFlag_;
    void cambiarOla(int wi);
};

#endif // MAINWINDOW_H
