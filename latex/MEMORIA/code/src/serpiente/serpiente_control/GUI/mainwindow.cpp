#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <ros/ros.h>
#include <qdebug.h>
#include <dialog.h>
#include <qcolordialog.h>
#include "iostream"
#include "ros/ros.h"
#include <stdlib.h>
#include <string.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //int argc; // Player Main() method does not take argument
    //char **argv;

    //ros::init(argc, argv, "serp_control");

    ROS_INFO("Empieza Nodo Control Serpiente");

    connect( ui->btn_parar, SIGNAL( clicked() ),
             this, SLOT( killLoop ()) );
    killLoopFlag_ = false;

    Wave wave1[] = {
        //  T     A   O    PD      PHASE0
        {2000, 0,  0,  0,      0},  //-- rodar
    };

    Wave wave2[] = {
        //  T     A   O    PD      PHASE0
        {2000, 0,  0,  0,      0},   //-- rodar
    };

    std::string inicioTopico = "/serp/box_cilinder_joint";
    std::string finalTopico = "_position_controller/command";
    std::string topico1 = inicioTopico+"1"+finalTopico;
    gusanoVer.add_servo(topico1);
    std::string topico2 = inicioTopico+"2"+finalTopico;
    gusanoHor.add_servo(topico2);
    std::string topico3 = inicioTopico + "3"+finalTopico;
    gusanoVer.add_servo(topico3);
    std::string topico4 = inicioTopico+"4"+finalTopico;
    gusanoHor.add_servo(topico4);
    std::string topico5 = inicioTopico+"5"+finalTopico;
    gusanoVer.add_servo(topico5);
    std::string topico6 = inicioTopico+"6"+finalTopico;
    gusanoHor.add_servo(topico6);
    std::string topico7 = inicioTopico+"7"+finalTopico;
    gusanoVer.add_servo(topico7);
    std::string topico8 = inicioTopico+"8"+finalTopico;
    gusanoHor.add_servo(topico8);
    std::string topico9 = inicioTopico+"9"+finalTopico;
    gusanoVer.add_servo(topico9);



    //-- numeros de waves definidas
    //-- numero de filas de la estructura wave
    //const int NWAVES = sizeof(wave1)/sizeof(Wave);

    wi=0;

    gusanoVer.set_wave(wave1[wi]);
    gusanoHor.set_wave(wave2[wi]);

    invertido = false;
    ROS_INFO("Empieza Nodo Control Serpiente");






}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_rodar_clicked()
{
    wi = 0;
    this->cambiarOla(wi);

}

void MainWindow::on_btn_rotar_clicked()
{
    wi = 2;
    this->cambiarOla(wi);

}

void MainWindow::on_btn_lateral_clicked()
{
    wi = 1;
    this->cambiarOla(wi);
}

void MainWindow::on_btn_gusano_clicked()
{
    wi = 3;
    this->cambiarOla(wi);

}

void MainWindow::on_btn_conectar_clicked()
{
    killLoopFlag_ = false;
    ROS_INFO("Empieza Nodo Control Serpiente");
    ui->btn_conectar->setStyleSheet(" color: rgb(255, 0, 0)");

    while(ros::ok()){

        gusanoVer.refresh();
        gusanoHor.refresh();
        QApplication::processEvents();
        if ( killLoopFlag_ ) {
            break;
        }

    }

}

void MainWindow::on_btn_invertir_clicked()
{
    Wave wave_v[] = {
            //  T     A   O    PD      PHASE0
            {this->Tv, this->Av,  this->Ov,  this->Dfv, this->Fov},
        };

        Wave wave_h[] = {
            //  T     A   O    PD      PHASE0
            {this->Th, this->Ah,  this->Oh,  this->Dfh, this->Foh},
        };

    if (!invertido){
        gusanoVer.set_wave(wave_h[0]);
        gusanoHor.set_wave(wave_v[0]);
        invertido = true;
    }else{
        gusanoVer.set_wave(wave_v[0]);
        gusanoHor.set_wave(wave_h[0]);
        invertido = false;
    }


}

void MainWindow::on_btn_salir_clicked()
{
    ros::shutdown();
    QWidget::close();

}

void MainWindow::on_btn_enviar_clicked()
{
    if(ui->rb_ah->isChecked()){
        this->Ah = ui->le_a->text().toInt();
        gusanoHor.SetA(this->Ah);
    }else{
        this->Av = ui->le_a->text().toInt();
        gusanoVer.SetA(this->Av);
    }

    if(ui->rb_dfh->isChecked()){
        this->Dfh = ui->le_df->text().toInt();
        gusanoHor.SetPd(this->Dfh);
    }else{
        this->Dfv = ui->le_df->text().toInt();
        gusanoVer.SetPd(this->Dfv);
        qDebug()<<this->Dfv;
    }

    if(ui->rb_fh->isChecked()){
        this->Th = ui->le_f->text().toInt();
        gusanoHor.SetT(this->Th);
    }else{
        //qDebug()<<(ui->le_f->text().toFloat());
        this->Tv = ui->le_f->text().toInt();
        gusanoVer.SetT(this->Tv);
    }

    if(ui->rb_fih->isChecked()){
        gusanoHor.SetPh0(ui->le_fi->text().toInt());
    }else{
        gusanoVer.SetPh0(ui->le_fi->text().toInt());
    }

    if(ui->rb_oh->isChecked()){
        gusanoHor.SetO(ui->le_o->text().toInt());
    }else{
        gusanoVer.SetO(ui->le_o->text().toInt());
    }
}

void::MainWindow::killLoop(){
    killLoopFlag_ = true;
}

void MainWindow::cambiarOla(int wi){
    Wave wave1[] = {
            //  T     A   O    PD      PHASE0
            {2000, 20,  0,  0,      90},  //-- rodar
            {1000, 20,  0,   135,        0},  //-- lateral
            {1000, 20, 0,   0,      90},  //-- rotacion
            {1000, 15, 0,   60,      0},   //-- lombriz
        };

        Wave wave2[] = {
            //  T     A   O    PD      PHASE0
            {2000, 20,  0,  0,      0},   //-- rodar
            {1000, 40,  0,   135,        0},  //-- lateral
            {1000, 180/9, 0,   60,      0},  //-- rotacion
            {1000, 0, 0,   0,      0},  //-- lombriz
        };

        gusanoVer.set_wave(wave1[wi]);
        gusanoHor.set_wave(wave2[wi]);
        qDebug()<<wave1[wi].A;
        ui->le_a->setText(QString::number(wave1[wi].A));
        ui->le_df->setText(QString::number(wave1[wi].PD));
        ui->le_f->setText(QString::number(wave1[wi].T));
        ui->le_fi->setText(QString::number(wave1[wi].PHASE0));
        ui->le_o->setText(QString::number(wave1[wi].O));


}
