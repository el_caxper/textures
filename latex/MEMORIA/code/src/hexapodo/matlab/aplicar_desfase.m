 Fs = 100;
 t=(0:Fs)/Fs';
 x=estruc_patas_rec(1).coxa_completo;
 % Now phase delay by pi/4 radians
 samples = length(estruc_patas_rec(1).coxa_completo);
 f=linspace(-Fs,Fs,samples)';
 y = ifft(ifftshift(fftshift(fft(x)).*exp(-1i*Fs*(pi/1))));
 % Finally take the real part (or the imaginary if you want)
 plot(estruc_patas_rec(1).tiempo,real(x));
 hold on;
 plot(estruc_patas_rec(1).tiempo,real(y));
 
% %%
 
 x = estruc_patas_rec(1).coxa_completo;
 x_shift = zeros(1, numel(x));
 x_shift(1:2000) = x(1659:3658);
 plot(x);
 hold on
 plot(x_shift,'k');
 
 
%%
 
     % code
 clc
 % clear all
 close all
 f1=20;
 t1=1;
fc=100*f1;
 samples=1000;
 t=(0:samples-1)/fc;
 y=sin(2*pi*f1*t);
 t0=1000/fc;
 f=linspace(-fc/2,fc/2,samples);
 y_ff=fftshift(fft(y));
 y_ff2=exp(-j*2*pi*f*t0).*y_ff;
 y2=ifft(ifftshift(y_ff2));
 plot(t,abs(y2))
 hold on
 % plot(t,y,'r')
 grid on
 
 
%%

% Testing phase shift

N = length(estruc_patas_rec(1).coxa_rec);
dt = 2*pi/N;
t = (0:N-1)*dt;
x = estruc_patas_rec(1).coxa_rec';
plot(t,x,'r-'); hold on
X = fftshift(fft(x));

% Phase shift
theta = 40*dt;

p = (0:length(X)/2)*theta;
P = [p -fliplr(p)];
P=P(1:end-1);

XX = X.*exp(1i*P);
xx = real(ifft(ifftshift(XX)));

plot(t,xx);
hold off


%%

    [Pxy,Freq] = cpsd(x,xx);
    coP = real(Pxy);
    quadP = imag(Pxy);
    phase = atan2(coP,quadP);
%---------------------------

%%

% dt = 1/100;
% t = 0:dt:30;
y1 = x; h1 = hilbert(y1);
y2 = xx; h2 = hilbert(y2);
p1 = angle(h1); p2 = angle(h2); % Instantaneous phase (wrapped)
p = unwrap(p2)-unwrap(p1);      % Instantaneous phase difference

figure;
subplot(211);
plot(t,p1,'r',t,p2,'b');
subplot(212);
plot(t,p,'k');

%%
y=x; % so we know the period is 2*pi roughly 6.28
ac=xcorr(y,y);
[~,locs]=findpeaks(ac);
mean(diff(locs)*0.01)

%%

x2=0:0.0001:1;
y=abs(cos(5*2*pi*x2));
ac=xcorr(y,y);
[~,locs]=findpeaks(ac);
mean(diff(locs)*0.001)

plot(x2,y)


