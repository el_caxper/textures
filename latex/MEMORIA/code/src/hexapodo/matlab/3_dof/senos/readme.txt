This program performs a sine curve-fit for an input time history.

The main script is:  sinefind.m

The remaining scripts are supporting functions.

- Tom Irvine