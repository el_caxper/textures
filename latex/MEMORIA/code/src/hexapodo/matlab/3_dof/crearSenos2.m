function [ syn ] = crearSenos2( x1r,x2r,x3r,tiempo )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
nfr = length(x1r);
syn=zeros(length(tiempo),1);

for ie=1:nfr
	syn=syn+x1r(ie)*sin(x2r(ie)*tiempo-x3r(ie));
end

end

