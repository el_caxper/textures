bagselect = rosbag('hex_3dof_2016-05-30-16-49-17.bag');
bag_piernas_controller = select(bagselect,'Topic','/hexapodo/joint_states');
msgs = bag_piernas_controller.readMessages;
%%
numArt = 18;
tiempo = bagselect.MessageList.Time(1:end,:)-bagselect.StartTime;
matrizArticulaciones = zeros(length(msgs),numArt);

for i=1:length(msgs)
    matrizArticulaciones(i,:)=msgs{i}.Position;
end

for i=1:numArt
    plot(matrizArticulaciones(:,i))
    hold on
end
hold off
legend(msgs{1}.Name{1:numArt})

%%
inicio = 487%553;%540;
largo_apro = 235;
tiempo_art3= tiempo(inicio+86:end-100-largo_apro);
tiempo_art3 = linspace(tiempo_art3(1),tiempo_art3(end),length(tiempo_art3));

% periodo=sum(tiempo_art3(2:end)-tiempo_art3(1:end-1))/length(tiempo_art3);

Posicion_art3 = matrizArticulaciones(inicio+86:end-100-largo_apro,13);


plot(Posicion_art3)
legend('tibia')

%%

periodo = 0.01;
numPicos = 4;
% Recortar valores finales
Posicion_art3 = Posicion_art3(1:2629);
tiempo_art3= tiempo_art3(1:2629);

Fs3 = 1/periodo;
numeroMuestra = length(Posicion_art3);
vectorFreqs =  Fs3*(0:(numeroMuestra/2))/numeroMuestra;
fft_art3=fft((Posicion_art3-sum(Posicion_art3)/length(Posicion_art3))./(numeroMuestra/2));
moduloFreq = abs(fft_art3);
moduloFreq_mitad = moduloFreq(1:numeroMuestra/2+1);
[amplitudPicosArt3,indPicosArt3] = findpeaks(moduloFreq_mitad,'NPeaks',numPicos,'SortStr','descend');

plot(vectorFreqs',moduloFreq_mitad,vectorFreqs(indPicosArt3),amplitudPicosArt3,'o');
text(vectorFreqs(indPicosArt3)+.05,amplitudPicosArt3,[num2str(vectorFreqs(indPicosArt3)'),repmat(' hz',length(vectorFreqs(indPicosArt3)'),1)]);
xlim([0,3])


%%

angles3 = angle(fft_art3(1:numeroMuestra/2+1));

% tiem3 = tiempo_art3;
tiem3 = (0:periodo:periodo*(numeroMuestra-1));
signal3 =sum(Posicion_art3)/length(Posicion_art3)+ zeros(1,length(tiem3));
for i = 1:numPicos
    freq = vectorFreqs(indPicosArt3(i))
    amp=amplitudPicosArt3(i)
    signal3 =signal3 +amplitudPicosArt3(i)*sin(2*pi*vectorFreqs(indPicosArt3(i))*tiem3+angles3(indPicosArt3(i)));
end
% Valores a mano con fase 
freqPrincipal = 1.2172 %1.2222
signal3 = amplitudPicosArt3(1)*sin(2*pi*freqPrincipal*0.2*tiem3+angles3(indPicosArt3(1)))+...
    amplitudPicosArt3(2)*sin(2*pi*freqPrincipal*0.6*tiem3+angles3(indPicosArt3(2)))+...
    amplitudPicosArt3(3)*sin(2*pi*freqPrincipal*tiem3+angles3(indPicosArt3(3)));

freqPrincipal = 0.247
signal3 = amplitudPicosArt3(1)*1.4*sin(2*pi*freqPrincipal*tiem3+angles3(indPicosArt3(1)))+...
    amplitudPicosArt3(2)*1.3*sin(2*pi*freqPrincipal*3*tiem3+angles3(indPicosArt3(2)))+...
    amplitudPicosArt3(3)*1*sin(2*pi*freqPrincipal*5*tiem3+angles3(indPicosArt3(3)))+...
    amplitudPicosArt3(4)*1*sin(2*pi*freqPrincipal*4*tiem3+angles3(indPicosArt3(4)));


plot(tiem3+tiempo_art3(1),signal3)
hold on
plot(tiempo_art3,Posicion_art3)
hold off
