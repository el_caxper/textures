
subplot(311)
plot(estruc_patas_rec(6).tiempo,estruc_patas_rec(6).coxa,...
    estruc_patas_rec(6).tiempo,estruc_patas_rec(6).coxa_completo);
title('coxa')
ylabel('amplitud (rad)')
legend('señal original','señal reconstruida');

subplot(312)
plot(estruc_patas_rec(6).tiempo(34:end)-estruc_patas_rec(6).tiempo(34)...
    +estruc_patas_rec(6).tiempo(1),estruc_patas_rec(6).femur(34:end),...
    estruc_patas_rec(6).tiempo,estruc_patas_rec(6).nuevo_femur+0.01);
title('coxa')
ylabel('amplitud (rad)')
legend('señal original','señal reconstruida');

subplot(313)
plot(estruc_patas_rec(6).tiempo(34:end)-estruc_patas_rec(6).tiempo(34)...
    +estruc_patas_rec(6).tiempo(1),estruc_patas_rec(6).tibia(34:end),...
    estruc_patas_rec(6).tiempo,estruc_patas_rec(6).nuevo_tibia+0.01);
title('coxa')
ylabel('amplitud (rad)')
legend('señal original','señal reconstruida');