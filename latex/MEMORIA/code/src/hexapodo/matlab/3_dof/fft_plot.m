periodo = 0.01;
numPicos = 3;
pierna =6 ;

Fs1 = round(1/periodo);
n1 = length(estruc_patas_rec(pierna).coxa_rec);
f1 =  Fs1*(0:(n1/2))/n1;
fft1 =fft(estruc_patas_rec(pierna).coxa_rec-sum(estruc_patas_rec(pierna).coxa_rec)/length(estruc_patas_rec(pierna).coxa_rec))./n1;
P1 = abs(fft1);
[pks1,locs1] = findpeaks(P1(1:n1/2+1),'NPeaks',numPicos,'SortStr','descend');

Fs2 = round(1/periodo);
n2 = length(estruc_patas_rec(pierna).femur);
f2 =  Fs2*(0:(n2/2))/n2;
fft2 = fft(estruc_patas_rec(pierna).femur-sum(estruc_patas_rec(pierna).femur)/length(estruc_patas_rec(pierna).femur))./n2;
P2 = abs(fft2);
[pks2,locs2] = findpeaks(P2(1:n2/2+1),'NPeaks',numPicos,'SortStr','descend');

Fs3 = round(1/periodo);
n3 = length(estruc_patas_rec(pierna).tibia);
f3 =  Fs3*(0:(n3/2))/n3;
fft3=fft(estruc_patas_rec(pierna).tibia-sum(estruc_patas_rec(pierna).tibia)/length(estruc_patas_rec(pierna).tibia))./n3;
P3 = abs(fft3);
[pks3,locs3] = findpeaks(P3(1:n3/2+1),'NPeaks',numPicos,'SortStr','descend');


sp1 = subplot(131);
plot(f1',P1(1:n1/2+1),f1(locs1),pks1,'o')
t1 = text(f1(locs1)+.05,pks1,[num2str(f1(locs1)'),repmat(' hz',length(f1(locs1)'),1)],'Parent', sp1);
set(t1, 'HorizontalAlignment', 'left');
title('coxa')
xlim([0,6])
xlabel('frecuencia (hz)');

sp2 = subplot(132);
plot(f2',P2(1:n2/2+1),f2(locs2),pks2,'o');
t2 = text(f2(locs2)+.05,pks2,[num2str(f2(locs2)'),repmat(' hz',length(f2(locs2)'),1)],'Parent', sp2);
set(t2, 'HorizontalAlignment', 'left');
title('femur')
xlim([0,6])
xlabel('frecuencia (hz)');

sp3 = subplot(133);
plot(f3',P3(1:n3/2+1),f3(locs3),pks3,'o');
t3 = text(f3(locs3)+.05,pks3,[num2str(f3(locs3)'),repmat(' hz',length(f3(locs3)'),1)],'Parent', sp3);
set(t3, 'HorizontalAlignment', 'left');
title('tibia')
xlim([0,6])
xlabel('frecuencia (hz)');


%%

angles1 = angle(fft1(1:round(n1/2)+1));
angles2 = angle(fft2(1:round(n2/2)+1));
angles3 = angle(fft3(1:round(n3/2)+1));

subplot(311)
plot(angles1)
subplot(312)
plot(angles2)
subplot(313)
plot(angles3)