coxa_rr = matrizArticulaciones(:,6);
femur_rr = matrizArticulaciones(:,12);
tarsus_rr = matrizArticulaciones(:,18);
tibia_rr =  matrizArticulaciones(:,24);
tiempo_rr = 0:0.01:(length(coxa_rr)-1)*0.01;

plot(tiempo_rr,coxa_rr,tiempo_rr,femur_rr, ...
    tiempo_rr,tibia_rr,tiempo_rr,tarsus_rr);
legend('coxa','femur','tibia','tarsus')
%%

muestras = 1:length(coxa_rr);
plot(muestras,coxa_rr,muestras,femur_rr, ...
    muestras,tibia_rr,muestras,tarsus_rr);
legend('coxa','femur','tibia','tarsus')

%%
for i = 1:size(matrizArticulaciones,2)/4
    estruc_patas(i).coxa = matrizArticulaciones(:,i);
    estruc_patas(i).femur = matrizArticulaciones(:,i+6);
    estruc_patas(i).tarsus = matrizArticulaciones(:,i+12);
    estruc_patas(i).tibia = matrizArticulaciones(:,i+18);
    estruc_patas(i).tiempo = [0:0.01:(length(coxa_rr)-1)*0.01]';
end
estruc_patas(1).patas = 'LF';
estruc_patas(2).patas = 'LM';
estruc_patas(3).patas = 'LR';
estruc_patas(4).patas = 'RF';
estruc_patas(5).patas = 'RM';
estruc_patas(6).patas = 'RR';

%%
star = 869;
final =4526;
for i = 1:size(matrizArticulaciones,2)/4
    estruc_patas_rec(i).coxa = estruc_patas(i).coxa(star:final);
    estruc_patas_rec(i).femur = estruc_patas(i).femur(star:final);
    estruc_patas_rec(i).tarsus = estruc_patas(i).tarsus(star:final);
    estruc_patas_rec(i).tibia = estruc_patas(i).tibia(star:final);
    estruc_patas_rec(i).tiempo = estruc_patas(i).tiempo(star:final);
end
[estruc_patas_rec(1:numel(estruc_patas)).patas]=estruc_patas.patas;

subplot(221)
for i = 1:size(matrizArticulaciones,2)/4
    plot(estruc_patas_rec(i).coxa);
    hold on;
end
legend(estruc_patas_rec.patas)
hold off;
subplot(222)
for i = 1:size(matrizArticulaciones,2)/4
    plot(estruc_patas_rec(i).femur);
    hold on;
end
hold off;

subplot(223)
for i = 1:size(matrizArticulaciones,2)/4
    plot(estruc_patas_rec(i).tarsus);
    hold on;
end
hold off;

subplot(224)
for i = 1:size(matrizArticulaciones,2)/4
    plot(estruc_patas_rec(i).tibia);
    hold on;
end
hold off;


%%

descanso1= 0.395;

for i = 1:size(matrizArticulaciones,2)/4
    indDescanso_arriba = find(estruc_patas_rec(i).coxa>descanso1);
    indDescanso_abajo = find(estruc_patas_rec(i).coxa<-descanso1);
    estruc_patas_rec(i).indDescanso(1).abajo = indDescanso_abajo;
    estruc_patas_rec(i).indDescanso(1).arriba = indDescanso_arriba;
    temp = estruc_patas_rec(i).coxa;
    temp(abs(estruc_patas_rec(i).coxa)>descanso1)=[];
    estruc_patas_rec(i).coxa_rec = temp;
    estruc_patas_rec(i).tiempo_rec = [0:0.01:(length(estruc_patas_rec(i).coxa_rec)-1)*0.01]';
end

subplot(221)
for i = 1:size(matrizArticulaciones,2)/4
    plot(estruc_patas_rec(i).coxa_rec);
    hold on;
end
legend(estruc_patas_rec.patas)
hold off;
subplot(222)
for i = 1:size(matrizArticulaciones,2)/4
    plot(estruc_patas_rec(i).femur);
    hold on;
end
hold off;

subplot(223)
for i = 1:size(matrizArticulaciones,2)/4
    plot(estruc_patas_rec(i).tarsus);
    hold on;
end
hold off;

subplot(224)
for i = 1:size(matrizArticulaciones,2)/4
    plot(estruc_patas_rec(i).tibia);
    hold on;
end
hold off;


%% Ajuste

for i = 1:length(estruc_patas_rec)
    [estruc_patas_rec(i).resultados(1).coxa, ...
        estruc_patas_rec(i).resultados(2).coxa, ...
        estruc_patas_rec(i).resultados(3).coxa] = ...
        mi_find_senos([estruc_patas_rec(i).tiempo_rec,...
        estruc_patas_rec(i).coxa_rec]);
    
    [estruc_patas_rec(i).resultados(1).femur, ...
        estruc_patas_rec(i).resultados(2).femur, ...
        estruc_patas_rec(i).resultados(3).femur] = ...
        mi_find_senos([estruc_patas_rec(i).tiempo ,...
        estruc_patas_rec(i).femur]);
    
    [estruc_patas_rec(i).resultados(1).tarsus, ...
        estruc_patas_rec(i).resultados(2).tarsus, ...
        estruc_patas_rec(i).resultados(3).tarsus] = ...
        mi_find_senos([estruc_patas_rec(i).tiempo,...
        estruc_patas_rec(i).tarsus]);
    
    [estruc_patas_rec(i).resultados(1).tibia, ...
        estruc_patas_rec(i).resultados(2).tibia, ...
        estruc_patas_rec(i).resultados(3).tibia] = ...
        mi_find_senos([estruc_patas_rec(i).tiempo,...
        estruc_patas_rec(i).tibia]);
end


%% Generar seniales

for i = 1:length(estruc_patas_rec)
    estruc_patas_rec(i).nuevo_coxa = crearSenos2(estruc_patas_rec(i).resultados(1).coxa,...
        estruc_patas_rec(i).resultados(2).coxa,estruc_patas_rec(i).resultados(3).coxa,...
        estruc_patas_rec(i).tiempo_rec);

    estruc_patas_rec(i).nuevo_femur = crearSenos2(estruc_patas_rec(i).resultados(1).femur,...
        estruc_patas_rec(i).resultados(2).femur,estruc_patas_rec(i).resultados(3).femur,...
        estruc_patas_rec(i).tiempo);
    
    estruc_patas_rec(i).nuevo_tarsus = crearSenos2(estruc_patas_rec(i).resultados(1).tarsus,...
        estruc_patas_rec(i).resultados(2).tarsus,estruc_patas_rec(i).resultados(3).tarsus,...
        estruc_patas_rec(i).tiempo);
    
    estruc_patas_rec(i).nuevo_tibia = crearSenos2(estruc_patas_rec(i).resultados(1).tibia,...
        estruc_patas_rec(i).resultados(2).tibia,estruc_patas_rec(i).resultados(3).tibia,...
        estruc_patas_rec(i).tiempo);
end

%% Aniadir descansos
for i = 1:length(estruc_patas_rec)
    Vector=estruc_patas_rec(i).nuevo_coxa';
    Idx=[estruc_patas_rec(i).indDescanso(1).arriba;estruc_patas_rec(i).indDescanso(1).abajo]';
    c=false(1,length(Vector)+length(Idx));
    c(Idx)=true;
    result=nan(size(c));
    result(~c)=Vector;

    estruc_patas_rec(i).coxa_completo = result;
    for j = 1:length(c)
        if c(j)==1 && sum(estruc_patas_rec(i).indDescanso(1).abajo==j)
            resultado(j) = estruc_patas_rec(i).nuevo_coxa(...
         estruc_patas_rec(i).indDescanso(1).abajo(1));
            estruc_patas_rec(i).coxa_completo(j) = -0.396;
        end
        if c(j)==1 && sum(estruc_patas_rec(i).indDescanso(1).arriba==j)
             resultado(j) = estruc_patas_rec(i).nuevo_coxa(...
         estruc_patas_rec(i).indDescanso(1).arriba(1));
     estruc_patas_rec(i).coxa_completo(j) = 0.396;
        end
%         estruc_patas_rec(i).coxa_completo(j) = result;
    end
    estruc_patas_rec(i).coxa_completo = estruc_patas_rec(i).coxa_completo';
end


%%

for i = 1:length(estruc_patas_rec)
    estruc_patas_rec(i).nuevo_coxa2 = [estruc_patas_rec(i).coxa_completo(57:end);
        estruc_patas_rec(i).coxa_completo(1:56)];
end

