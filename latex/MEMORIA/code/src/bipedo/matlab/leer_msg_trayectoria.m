bagselect = rosbag('piernas_8pasos_2016-05-07-13-32-24.bag');
bag_piernas_controller = select(bagselect,'Topic','/piernas/piernas_controller/state');
msgs = bag_piernas_controller.readMessages;

%%
% subplot(211)
matrizArticulaciones = zeros(length(msgs),12);

for i=1:length(msgs)
    matrizArticulaciones(i,:)=msgs{i}.Actual.Positions;
end

for i=[4 10]
    plot(matrizArticulaciones(:,i))
    hold on
end
% plot(matrizArticulaciones(:,4))
legend(msgs{1}.JointNames{1:6})
hold on


%%
% matrizArticulaciones = zeros(length(msgs),12);
%
% for i=1:length(msgs)
%     matrizArticulaciones(i,:)=msgs{i}.Actual.Positions;
% end
%
% % for i=1:6
% %     plot(matrizArticulaciones(:,i))
% %     hold on
% % end
% plot(matrizArticulaciones(:,4))
% legend(msgs{1}.JointNames{1:6})
% hold off

%%

matrizPasosNormales = matrizArticulaciones(1114:end-300,:);
tiempo = bagselect.MessageList.Time(1114:end-300,:)-bagselect.StartTime;
periodo=sum(bagselect.MessageList.Time(2:end)-bagselect.MessageList.Time(1:end-1))/length(bagselect.MessageList.Time);
matrizPasosRecortados = [];
numPasos = 8
longitudZonaMuertas = 443%443;
longitudMovimientos = 1420%1398;


for k =1:numPasos
    matrizPasosRecortados = [matrizPasosRecortados; ...
        matrizPasosNormales(longitudMovimientos*(k-1)+longitudZonaMuertas:longitudMovimientos*k,:)];
    %     plot(matrizPasosRecortados(:,1))
end

for i=1:6
    plot(tiempo-tiempo(1) , matrizPasosNormales(:,i))
    hold on
end
% plot(matrizArticulaciones(:,4))
% legend(msgs{1}.JointNames{1:6})
legend('art1','art2','art3','art4','art5','art6')

hold off


%%

zonaMuerta1= 0.0017;
zonaMuerta2= -0.3008;
zonaMuerta3d= -0.3022;
zonaMuerta3u= -0.3;
zonaMuerta4= 0.6027;
zonaMuerta5= 0.2977;
zonaMuerta6d= -0.3028;
zonaMuerta6u= -0.2983;

art1 = matrizPasosNormales(:,1);
art1(art1 < zonaMuerta1) = [];
art2 = matrizPasosNormales(:,2);
art2(art2 < zonaMuerta2) = [];
art3 = matrizPasosNormales(:,3);
art3(art3 > zonaMuerta3d & art3 < zonaMuerta3u) = [];
art4 = matrizPasosNormales(:,4);
art4(art4 < zonaMuerta4) = [];
art5 = matrizPasosNormales(:,5);
art5(art5 > zonaMuerta5) = [];
art6 = matrizPasosNormales(:,6);
art6(art6 > zonaMuerta6d & art6 < zonaMuerta6u) = [];

tiempo_art1 = 0:0.01:(length(art1)-1)*0.01;
tiempo_art2 = 0:0.01:(length(art2)-1)*0.01;
tiempo_art3 = 0:0.01:(length(art3)-1)*0.01;
tiempo_art4 = 0:0.01:(length(art4)-1)*0.01;
tiempo_art5 = 0:0.01:(length(art5)-1)*0.01;
tiempo_art6 = 0:0.01:(length(art6)-1)*0.01;
plot(tiempo_art1,art1);
hold on
plot(tiempo_art2,art2);
plot(tiempo_art3,art3);
plot(tiempo_art4,art4);
plot(tiempo_art5,art5);
plot(tiempo_art6,art6);
hold off
title('Posiciones de las articulaciones')
xlabel('tiempo (s)')
ylabel('rads')
legend('art1','art2','art3','art4','art5','art6')
%%
periodo = 0.01;
numPicos = 4;

Fs1 = round(1/periodo);
n1 = length(art1);
f1 =  Fs1*(0:(n1/2))/n1;
fft1 =fft(art1-sum(art1)/length(art1))./n1;
P1 = abs(fft1);
[pks1,locs1] = findpeaks(P1(1:n1/2+1),'NPeaks',numPicos);

Fs2 = round(1/periodo);
n2 = length(art2);
f2 =  Fs2*(0:(n2/2))/n2;
fft2 = fft(art2-sum(art2)/length(art2))./n2;
P2 = abs(fft2);
[pks2,locs2] = findpeaks(P2(1:n2/2+1),'NPeaks',numPicos);

Fs3 = round(1/periodo);
n3 = length(art3);
f3 =  Fs3*(0:(n3/2))/n3;
fft3=fft(art3-sum(art3)/length(art3))./n3;
P3 = abs(fft3);
[pks3,locs3] = findpeaks(P3(1:n3/2+1),'NPeaks',numPicos);

Fs4 = round(1/periodo);
n4 = length(art4);
f4 =  Fs4*(0:(n4/2))/n4;
fft4 =fft(art4-sum(art4)/length(art4))./n4;
P4 = abs(fft4);
[pks4,locs4] = findpeaks(P4(1:n4/2+1),'NPeaks',numPicos);

Fs5 = round(1/periodo);
n5 = length(art5);
f5 =  Fs5*(0:(n5/2))/n5;
fft5 = fft(art5-sum(art5)/length(art5))./n5;
P5 = abs(fft5);
[pks5,locs5] = findpeaks(P5(1:n5/2+1),'NPeaks',numPicos);

Fs6 = round(1/periodo);
n6 = length(art6);
f6 =  Fs6*(0:(n6/2))/n6;
fft6 = fft(art6-sum(art6)/length(art6))./n6;
P6 = abs(fft6);
[pks6,locs6] = findpeaks(P6(1:n6/2+1),'NPeaks',numPicos);

sp1 = subplot(231);
plot(f1',P1(1:n1/2+1),f1(locs1),pks1,'o')
t1 = text(f1(locs1)+.05,pks1,[num2str(f1(locs1)'),repmat(' hz',length(f1(locs1)'),1)],'Parent', sp1);
set(t1, 'HorizontalAlignment', 'left');
title('Art1')
xlim([0,3])

sp2 = subplot(232);
plot(f2',P2(1:n2/2+1),f2(locs2),pks2,'o');
t2 = text(f2(locs2)+.05,pks2,[num2str(f2(locs2)'),repmat(' hz',length(f2(locs2)'),1)],'Parent', sp2);
set(t2, 'HorizontalAlignment', 'left');
title('Art2')
xlim([0,3])

sp3 = subplot(233);
plot(f3',P3(1:n3/2+1),f3(locs3),pks3,'o');
t3 = text(f3(locs3)+.05,pks3,[num2str(f3(locs3)'),repmat(' hz',length(f3(locs3)'),1)],'Parent', sp3);
set(t3, 'HorizontalAlignment', 'left');
title('Art3')
xlim([0,3])

sp4 = subplot(234);
plot(f4',P4(1:n4/2+1),f4(locs4),pks4,'o');
t4 = text(f4(locs4)+.05,pks4,[num2str(f4(locs4)'),repmat(' hz',length(f4(locs4)'),1)],'Parent', sp4);
set(t4, 'HorizontalAlignment', 'left');
title('Art4')
xlim([0,3])

sp5 = subplot(235);
plot(f5',P5(1:n5/2+1),f5(locs5),pks5,'o');
t5 = text(f5(locs5)+.05,pks5,[num2str(f5(locs5)'),repmat(' hz',length(f5(locs5)'),1)],'Parent', sp5);
set(t5, 'HorizontalAlignment', 'left');
title('Art5')
xlim([0,3])

sp6 = subplot(236);
plot(f6',P6(1:n6/2+1),f6(locs6),pks6,'o');
t6 = text(f1(locs6)+.05,pks6,[num2str(f6(locs6)'),repmat(' hz',length(f6(locs6)'),1)],'Parent', sp6);
set(t6, 'HorizontalAlignment', 'left');
title('Art6')
xlim([0,3])

%%

angles1 = angle(fft1(1:round(n1/2)+1));
angles2 = angle(fft2(1:round(n2/2)+1));
angles3 = angle(fft3(1:round(n3/2)+1));
angles4 = angle(fft4(1:round(n4/2)+1));
angles5 = angle(fft5(1:round(n5/2)+1));
angles6 = angle(fft6(1:round(n6/2)+1));

tiem1 = 1:periodo:70;
signal1 = sum(art1)/length(art1)+zeros(1,length(tiem1));
for i = 1:numPicos
    locs1(i)
    signal1 =signal1 + 2*pks1(i)*sin(2*pi*f1(locs1(i))*tiem1+angles1(locs1(i)));
end
plot(signal1)
hold on
plot(art1-sum(art1)/length(art1)./n1)
%
% tiem2 = 1:periodo:70;
% signal2 =sum(art2)/length(art2)+ zeros(1,length(tiem2));
% for i = 1:4
%     signal2 =signal2 + 2*pks2(i)*sin(2*pi*f2(locs2(i))*tiem2+0*angles2(locs2(i)));
% end
% plot(signal2)
% hold on
% plot(art2-sum(art2)/length(art2)./n2)
% hold off
% tiem3 = 1:periodo:140;
% signal3 = zeros(1,length(tiem3));
% for i = 1:4
%     signal3 =signal3 + pks3(i)*sin(2*pi*f3(locs3(i))*tiem3+angles3(locs3(i)));
% end
% plot(signal3)
% hold on
% plot(art3-sum(art3)/length(art3)./n3)
%
% tiem4 = 1:periodo:140;
% signal4 = zeros(1,length(tiem4));
% for i = 1:4
%     signal4 =signal4 + pks4(i)*sin(2*pi*f4(locs4(i))*tiem4+angles4(locs4(i)));
% end
% plot(signal4)
% hold on
% plot(art4-sum(art4)/length(art4)./n4)

%
% tiem5 = 1:periodo:140;
% signal5 = zeros(1,length(tiem5));
% for i = 1:4
%     signal5 =signal5 + pks5(i)*sin(2*pi*f5(locs5(i))*tiem5+angles5(locs5(i)));
% end
% plot(signal5)
% hold on
% plot(art5-sum(art5)/length(art5)./n5)
%
% tiem6 = 1:periodo:140;
% signal6 = zeros(1,length(tiem6));
% for i = 1:4
%     signal6 =signal6 + pks6(i)*sin(2*pi*f6(locs6(i))*tiem6+angles6(locs6(i)));
% end
% plot(signal6)
% hold on
% plot(art6-sum(art6)/length(art6)./n6)

%%
% tiem1 = 1:periodo:70;
% [sortedP1,sortingIndices1] = sort(P1(1:n1/2+1),'descend');
%
% signal1 = zeros(1,length(tiem1));
% for i = 1:500
%     signal1 =signal1 + sortedP1(i)*sin(2*pi*f1(sortingIndices1(i))*tiem1+angles1(sortingIndices1(i)));
% end
% plot(signal1)
% hold on
% plot(art1-sum(art1)/length(art1)./n1)
% hold off
tiem1 = 1:periodo:70;
[sortedP2,sortingIndices2] = sort(P2(1:n2/2+1),'descend');

signal1 = zeros(1,length(tiem1));
for i = 1:10
    signal1 =signal1 + sortedP2(i)*sin(2*pi*f2(sortingIndices2(i))*tiem1+angles2(sortingIndices2(i)));
end
plot(signal1)
hold on
plot(art2-sum(art2)/length(art2)./n2)
