bagselect = rosbag('piernas_girando_mas_2016-05-17-11-22-09.bag')
% bag_piernas_controller = select(bagselect,'Topic','/piernas/piernas_controller/state')
% msgs = bag_piernas_controller.readMessages;
gazebo_links = select(bagselect,'Topic','/gazebo/link_states');
seleccion_gazebo = gazebo_links.readMessages(1:200:gazebo_links.NumMessages);
%%
for i = 1:length(seleccion_gazebo)
    pose = [seleccion_gazebo{i,1}.Pose(2).Position.X, ...
            seleccion_gazebo{i,1}.Pose(2,1).Position.Y, ...
            seleccion_gazebo{i,1}.Pose(2,1).Position.Z];
    for j = 3:8 %length(seleccion_gazebo{i,1}.Pose)
        punto = [seleccion_gazebo{i,1}.Pose(j).Position.X, ...
            seleccion_gazebo{i,1}.Pose(j,1).Position.Y, ...
            seleccion_gazebo{i,1}.Pose(j,1).Position.Z];
        pose = [pose;punto];
    end
    plot3(pose(:,1),pose(:,2),pose(:,3));
    hold on
    pose = [seleccion_gazebo{i,1}.Pose(2).Position.X, ...
            seleccion_gazebo{i,1}.Pose(2,1).Position.Y, ...
            seleccion_gazebo{i,1}.Pose(2,1).Position.Z];
    for j = 9:14 %length(seleccion_gazebo{i,1}.Pose)
        punto = [seleccion_gazebo{i,1}.Pose(j).Position.X, ...
            seleccion_gazebo{i,1}.Pose(j,1).Position.Y, ...
            seleccion_gazebo{i,1}.Pose(j,1).Position.Z];
        pose = [pose;punto];
    end
    plot3(pose(:,1),pose(:,2),pose(:,3));
    hold on
end
view([90 0])
hold off
axis equal