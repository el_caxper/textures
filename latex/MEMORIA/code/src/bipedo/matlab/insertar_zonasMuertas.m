% for i = 1:length(estruc_piernaDer_fase)
%     Vector=estruc_piernaDer_fase(i).senial_recons;
%     Idx=estruc_piernaDer_fase(i).indZonamuerta;
%     c=false(1,length(Vector)+length(Idx));
%     c(Idx)=true;
%     result=nan(size(c));
%     result(~c)=Vector;
%     % result(c)=zonaMuerta1*ones(1,length(indZonamuerta1));
%     result(c)=estruc_piernaDer_fase(i).senial_recons(...
%         estruc_piernaDer_fase(i).indZonamuerta(1))...
%         *ones(1,length(estruc_piernaDer_fase(i).indZonamuerta));
%     estruc_piernaDer_fase(i).senialCompleta = result;
% end
% % 
% % 
% for i = 1:length(estruc_piernaDer_fase)
%     subplot(2,3,i)
%     plot(estruc_piernaDer_fase(i).tiempo_completo,estruc_piernaDer_fase(i).senialCompleta,...
%         estruc_piernaDer_fase(i).tiempo_completo, estruc_piernaDer_fase(i).completa);
%     title(['Art',num2str(i)]);
%     xlim([0 130]);
%     legend('recon','orig');
% %     plot(estrucArt(i).tiempo_completo,estrucArt(i).senialCompleta);
% end

%%
for i = 1:length(estruc_piernaDer_fase)
    subplot(2,3,i)
    plot(estruc_piernaDer_fase(i).tiempo_completo,estruc_piernaDer_fase(i).senialCompleta,...
        estruc_piernaIzq_fase(i).tiempo_completo, estruc_piernaIzq_fase(i).senialCompleta);
    title(['Art',num2str(i)]);
    xlim([0 130]);
    legend('der','izq');
%     plot(estrucArt(i).tiempo_completo,estrucArt(i).senialCompleta);
end

% hold on
% for i = 1:length(estruc_piernaDer)
% %     plot(estrucArt(i).tiempo_completo,estrucArt(i).completa,...
% %         estrucArt(i).tiempo_completo,estrucArt(i).senialCompleta);
%     plot(estruc_piernaDer(i).tiempo_completo(323:end),estruc_piernaDer(i).senialCompleta(1:end-322));
%     hold on
% end
% hold off
%%

for i = 2:length(estruc_piernaDer_fase)
    estruc_piernaIzq_fase(i).senialCompleta =estruc_piernaDer_fase(i).senialCompleta(703:end);
    estruc_piernaIzq_fase(i).tiempo_completo =estruc_piernaDer_fase(i).tiempo_completo(703:end)-...
        estruc_piernaDer_fase(i).tiempo_completo(703);
    estruc_piernaDer_fase(i).tiempo_completo = estruc_piernaDer_fase(i).tiempo_completo;
    estruc_piernaDer_fase(i).senialCompleta = estruc_piernaDer_fase(i).senialCompleta;
    
end

%%
temp = estruc_piernaIzq_fase(2);
estruc_piernaIzq_fase(2) = estruc_piernaIzq_fase(5);
estruc_piernaIzq_fase(5) = temp;

%%

for i = 1:length(estruc_piernaDer_fase)
    estruc_piernaDer_fase(i).senialCompleta = estruc_piernaDer_fase(i).senialCompleta...
        (1:length(estruc_piernaIzq_fase(i).senialCompleta));
    estruc_piernaDer_fase(i).tiempo_completo = estruc_piernaDer_fase(i).tiempo_completo...
        (1:length(estruc_piernaIzq_fase(i).senialCompleta));
    
end

