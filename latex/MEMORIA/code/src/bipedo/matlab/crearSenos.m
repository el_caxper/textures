function [ matrizFunciones ] = crearSenos( funciones,matrizTiempos)
for i = 1:length(funciones)
    tiempo = matrizTiempos(i).tiempo;
    compCont = funciones(i).componenteContinua;
    amp = funciones(i).amplitudes;
    fase = funciones(i).fases;
    freq = funciones(i).freq;
    matrizFunciones(i).senial = zeros(1,length(tiempo))+compCont;
    for ie=1:length(funciones(i).freq)
        %
        matrizFunciones(i).senial=matrizFunciones(i).senial+amp(ie)*sin(freq(ie)*tiempo-fase(ie));
        %
    end
end

end