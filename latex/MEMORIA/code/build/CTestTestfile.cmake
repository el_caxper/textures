# CMake generated Testfile for 
# Source directory: /home/kaiser/WS_ROS/ws_tfg/src
# Build directory: /home/kaiser/WS_ROS/ws_tfg/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(manipulador/abb_irb_120_gazebo)
SUBDIRS(manipulador/nuestro_abb_irb120_peq)
SUBDIRS(manipulador/abb_irb_120_control)
SUBDIRS(manipulador/abb_irb_120_description)
SUBDIRS(bipedo/bipedo_control)
SUBDIRS(bipedo/bipedo_description)
SUBDIRS(bipedo/bipedo_gazebo)
SUBDIRS(cuadrupedo/cuadrupedo_control)
SUBDIRS(cuadrupedo/cuadrupedo_description)
SUBDIRS(cuadrupedo/cuadrupedo_gazebo)
SUBDIRS(hexapodo/hexapodo_control)
SUBDIRS(hexapodo/hexapodo_description)
SUBDIRS(hexapodo/hexapodo_gazebo)
SUBDIRS(robot_movil_4_ruedas/robot_movil_control)
SUBDIRS(robot_movil_4_ruedas/robot_movil_description)
SUBDIRS(robot_movil_4_ruedas/robot_movil_gazebo)
SUBDIRS(serpiente/serpiente_control)
SUBDIRS(serpiente/serpiente_description)
SUBDIRS(serpiente/serpiente_gazebo)
SUBDIRS(hrl_kinematics-hydro-devel)
SUBDIRS(manipulador/nuestro_abb_irb120)
