%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%			Capitulo 4				                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Descripción de la estructura de bloques del sistema.} \label{cap4}

En este capítulo se describe el sistema que se ha creado para la clasificación de texturas. La descripción del sistema se realiza desde un punto de vista estructural, para ellos se muestran los distintos bloques que forman el sistema y se detalla la relación entre ellos. La descripción funcional del sistema se describe en el siguiente capítulos.

\newpage

\section{Diagrama de Bloques}

	El sistema se ha diseñado de forma que sea lo más modular posible y que cada uno de los módulos sea independiente del resto. Debido a esto, el sistema, se ha dividido en cuatro módulos, cada uno de ellos se encarga de realizar una tarea determinada. La salida de los módulos se usa como entrada en el siguiente módulo como se muestra en la Figura~\ref{Figura4_01}. Para conseguir la persistencia de los datos se ha usado una base de datos \textit{sqlite} por lo que la salida de un módulo se guarda en una base de datos y el siguiente módulo va a buscar sus datos de entrada a esta base de datos.

	\begin{figure}[htb]
		\centering
		\includegraphics[width = 1\linewidth]{capitulo_04/figuras_dir/estructura_sistema.pdf}
		\caption{Estructura del sistema}
		\label{Figura4_01}
	\end{figure}
	
	Los bloques que forman el sistema se diferencian según la tarea que cada uno realiza, según esto la definición que se ha elegido es la siguiente:
	
	\begin{itemize}
		\item \textbf{Selección}: Este módulo se encarga de obtener las muestras de una imagen a través de la ayuda de un usuario. El módulo permite generar los datos que definen completamente una muestra, para ello el sistema permite obtener muestras a partir de imágenes \textit{png} y \textit{jpg}. Los parámetros generados que definen la muestra son los siguientes: 
		\begin{itemize}
			\item Etiqueta: nombre que identifica el tipo de textura al que hace referencia la muestra.
			\item Posición x: posición en el eje x del centro de la muestra.
			\item Posición y: posición en el eje y del centro de la muestra.
			\item Tamaño: tamaño en píxeles de la muestra.
			\item Positiva: indica si la muestra es positiva o negativa. Este dato es usado por el clasificador.
		\end{itemize}
		\item \textbf{Extracción}: Este bloque es el encargado de obtener las características de Hararick a partir de la matriz de co-ocurrencia obtenida de la muestra. Las características que se obtienen de cada muestra son las definidas en el apartado \ref{Haralick_features}. 
		\item \textbf{Entrenamiento}: Este bloque se encarga de entrenar un clasificador usando \ac{SVM}. Este módulo permite al usuario configurar los parámetros del clasificador.
		\item \textbf{Clasificador}: Este módulo se encarga de predecir las texturas que aparecen en una imagen proporcionada como entrada. El clasificador usado consiste en un SVM descrito en el capítulo~\ref{cap3}. Este clasificador se puede configurar para se usado con los diferentes kernels descritos en apartado~\ref{funciones_kernel}.
	\end{itemize}
	
\section{Conexión de los Bloques}

Aunque los módulos pueden ser usados de forma independiente, para un correcto funcionamiento del sistema es recomendable el uso de todos los módulos. La conexión entre los módulos es la siguiente.

\begin{itemize}
	\item \textbf{Selección}: Este bloque toma como entrada una serie de imágenes de las que se quieren obtener las muestras y como la salida proporciona un conjunto de datos que permiten definir la muestra, estos datos se almacenan en la base de datos.  
	\item \textbf{Extracción}: Este bloque toma como entrada las muestras que se han generado en bloque anterior y su salida son las características de Hararick. 
	\item \textbf{Entrenamiento}: Este bloque toma como entrada las características que se han generado en el bloque anterior y la salida es un clasificador \ac{SVM}.
	\item \textbf{Clasificador}: Este módulo toma como entrada un clasificador \ac{SVM} y una imagen y su salida consiste en una serie de datos que se muestran por pantalla con las texturas encontradas en la imagen, estos datos se pueden exportar a un fichero.
\end{itemize}

	En la Figura~\ref{Figura4_02} se muestra la conexión entre los distintos bloques y sus entradas y salidas. En esta imagen se muestran los datos que cada módulo guarda u obtiene de la base de datos.
	
	\begin{figure}[htb]
		\centering
		\includegraphics[width = 1\linewidth]{capitulo_04/figuras_dir/conexion_bloques.pdf}
		\caption{Conexión de los bloques}
		\label{Figura4_02}
	\end{figure}



\section{Base de datos}

	Todos los datos usados por el sistema son almacenados por una base de datos \textit{sqlite}. El módulo de selección es el encargado de crear la base de datos ya que es el primer módulo usado en el sistema. Esta base de datos se crea la carpeta donde se encuentren la imágenes de las que se van a obtener las muestras. El nombre por defecto de la base de datos es \textit{Smaple.db}.
	
	\subsection{Estructura de la base de datos}
	
	La base de datos cuenta con cuatro tablas que son la tabla \textit{Image}, tabla \textit{Sample}, tabla \textit{Feature} y tabla \textit{Classifier}. En la Figura~\ref{Figura4_03} se muestra el diagrama de relación (\ac{ERD}) de la base de datos.
	
	\begin{figure}[htb]
		\centering
		\includegraphics[width = 1\linewidth]{capitulo_04/figuras_dir/database_structure.pdf}
		\caption{Estructura de la base de datos}
		\label{Figura4_03}
	\end{figure}
	
	En la tabla \textit{Image} se encuentran las imágenes de las que se han obtenido muestras. Se añaden las filas a través del módulo de selección cuando se añade la primera muestra a una imagen, la imagen es añadida a la base de datos. A continuación se describen los campos de tabla \textit{Image}:
	\begin{itemize}
		\item IdImage: identificador autoincremental de cada una de las imágenes.
		\item ImageName: nombre de la imagen.
	\end{itemize}

	En la tabla \textit{Sample} se añaden las filas a través del módulo de selección cuando se añade una nueva muestra a una imagen, la muestra es añadida a la base de datos. A continuación se describen los campos de tabla \textit{Sample}:
	\begin{itemize}
		\item IdSample: identificador autoincremental de cada una de las muestras.
		\item IdImage: identificador de la imagen de la que se ha obtenido la muestra.
		\item Label: etiqueta que identifica la clase de la muestra.
		\item PosX: posición x en píxeles del centro de la muestra.
		\item PosY: posición y en píxeles del centro de la muestra.
		\item Size: tamaño en píxeles del cuadrado de la muestra.
		\item Positive: variable booleana que indica si la muestra es positiva o negativa.
	\end{itemize}

	En la tabla \textit{Feature} se añaden las filas a través del módulo de extracción cuando se obtienen las características de cada una de las muestras almacenadas en la base de datos. A continuación se describen los campos de tabla \textit{Feature}:
	\begin{itemize}
		\item IdFeature: identificador autoincremental de cada una de las filas de características.
		\item IdSample: identificador de la muestra de la que se han obtenido las características.
		\item Label: : etiqueta que identifica la clase de las características.
		\item ASM: valor calculado según la ecuación~\ref{ASM}
		\item Contrast: valor calculado según la ecuación~\ref{Contrast}
		\item Correlation: valor calculado según la ecuación~\ref{Correlation}
		\item SumOfSquareVariances: valor calculado según la ecuación~\ref{SumOfSquareVariances}
		\item InverseDifferentMoment: valor calculado según la ecuación~\ref{InverseDifferentMoment}
		\item SumAverage: valor calculado según la ecuación~\ref{SumAverage}
		\item SumVariance: valor calculado según la ecuación~\ref{SumVariance}
		\item SumEntropy: valor calculado según la ecuación~\ref{SumEntropy}
		\item Entropy: valor calculado según la ecuación~\ref{Entropy}
		\item DifferenceVariance: valor calculado según la ecuación~\ref{DifferenceVariance}
		\item DifferenceEntropy: valor calculado según la ecuación~\ref{DifferenceEntropy}
		\item MessureCorrelation1: valor calculado según la ecuación~\ref{MessureCorrelation1}
		\item MessureCorrelation2: valor calculado según la ecuación~\ref{MessureCorrelation2}
		\item MaximalCorrelationCoefficient: valor calculado según la ecuación~\ref{MaximalCorrelationCoefficient}
	\end{itemize}

	En la tabla \textit{Classifier} se añaden las filas a través del módulo de clasificación cuando se realiza el entrenamiento de un clasificador y si el usuario decide guardar este clasificador, existe la opción de almacenarlo en un fichero o en la tabla \textit{Classifier} de la base de datos. A continuación se describen los campos de tabla \textit{Classifier}:
	\begin{itemize}
		\item IdClassifier: identificador autoincremental de cada uno de los clasificadores almacenados.
		\item ClassifierName: nombre único que identifica el clasificador.
		\item ClassifierValue: clasificador almacenado como un objeto serializado.
	\end{itemize}


	
	\subsection{Acceso a la base de datos}
	Si varias de las aplicaciones están ejecutándose simultáneamente, estas tendrán que acceder a la base de datos de forma simultánea. Esto no produce ningún problema ya que cada aplicación solo escribe en una tabla y el resto las usa para leer. 
	
	Una aplicación bloquea la base de datos contra escritura mientras está realizando inserciones o actualizando una determinada tabla, no realiza un bloqueo mientras se esté ejecutando. Esto permite que varias aplicaciones se estén ejecutando de forma simultanea o se edite la base de datos de forma manual.
	
	En cambio si la base de datos es editada de forma manual a través de un \textit{DB Browser} se pueden producir incoherencias entre lo que se muestra en las aplicaciones y lo que hay en la base de datos. Para solucionar este problema es necesario recargar la base de datos en la aplicación aunque es recomendable cerrar y volver a abrir la aplicación.


\section{Flujo de trabajo}
%\vspace{-0.5cm}

A continuación se describen las etapas que forman el proceso de una forma secuencial. Estas etapas abarcan desde las etapas iniciales de preprocesado de las imágenes hasta la última etapa de clasificación.

La primera etapa consiste en un preprocesado de las imágenes que consta de dos etapas:
\begin{itemize}
	\item Transformación a escala de grises. Puesto que todo el proceso de análisis de texturas se realiza para imágenes en escala de grises y no en imágenes en color, el primer paso consiste en transformar la imagen en caso de que sea necesario.
	\item En segundo lugar se seleccionan las muestras según se ha descrito en el módulo de selección de muestras.
\end{itemize}

La segunda etapa consiste en la extracción de las características a partir de las muestras, para ello se realizan dos pasos:
\begin{itemize}
	\item Cálculo de la matriz de co-ocurrencia, para el cálculo de esta matriz es necesario que la imagen esté en blanco y negro. La matriz de co-ocurrencia se calcula según la ecuación \ref{coocurrence}.
	\item A partir de la matriz de co-ocurrencia se calculan las catorce características de Haralick definidas anteriormente en el apartado \ref{Haralick_features}.
\end{itemize}

La tercera etapa consiste en la selección de características. Para ello el módulo de clasificación permite seleccionar el número de características que se quieren usar. Para ello se usa \ac{PCA}.

La cuarta etapa consiste en el entrenamiento del clasificador que consiste en dos etapas:
\begin{itemize}
	\item Selección de los parámetros del clasificador, es necesario seleccionar el kernel del clasificador y los parámetros necesarios, los cuales aparecen descritos en \ref{funciones_kernel}.
	\item El dataset de entrenamiento es dividido en un conjunto de entrenamiento y uno de test. Tras el entrenamiento del clasificador se puede ver su resultado tras ser aplicado al conjunto de test.
\end{itemize}

La quinta y última etapa consiste en la clasificación, que consta de dos etapas:
\begin{itemize}
	\item En primer lugar es necesario seleccionar el tamaño de la muestra con el que se va a recorrer la imagen.
	\item Por último se recorre la imagen y se visualizan los resultados, en caso de que sea necesario los resultados pueden ser exportados a un pdf.
\end{itemize}


En la figura \ref{Figura4_04} se muestra un diagrama de flujo de las etapas descritas anteriormente.

\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.8\linewidth]{capitulo_04/figuras_dir/proceso_completo_2.pdf}
	\label{Figura4_04}
\end{figure}
