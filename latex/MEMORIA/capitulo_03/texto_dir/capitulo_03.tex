%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%			Capitulo 3					%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%http://www.ia.uned.es/~ejcarmona/publicaciones/[2013-Carmona]%20SVM.pdf 

\chapter{Tecnología de aprendizaje utilizada (SVM)}\label{cap3}
		
En este capítulo se muestra el método usado para la clasificación de texturas, que ha sido \textit{support vector machine}. Se explican desde un punto de vista matemático las características y funciones de este método que será usado más adelante en otros capítulos de la memoria.

\newpage
\section{Introducción}

Las máquinas de soporte vectorial (SVM, del inglés Support Vector Machines) tienen su origen en los trabajos sobre la teoría del aprendizaje estadístico y fueron introducidas en los años 90 por Vapnik y sus colaboradores \citep{boser1992training}, \citep{cortes1995support}. Aunque originariamente las SVMs fueron pensadas para resolver problemas de clasificación binaria, actualmente se utilizan para resolver otros tipos de problemas (regresión, agrupamiento y multiclasificación). También son diversos los campos en los que han sido utilizadas con éxito, tales como visión artificial, reconocimiento de caracteres, categorización de texto e hipertexto, clasificación de proteínas, procesamiento del lenguaje natural y análisis de series temporales. De hecho, desde su introducción, han ido ganando un merecido reconocimiento gracias a sus sólidos fundamentos teóricos. 

Dentro de la tarea de clasificación, los SVMs pertenecen a la categoría de los clasificadores lineales, puesto que inducen separadores lineales o hiperplanos, ya sea en el espacio original de los ejemplos de entrada, si éstos son separables o cuasi-separables (ruido), o en un espacio transformado (espacio de características), si los ejemplos no son separables linealmente en el espacio original. Como se verá más adelante, la búsqueda del hiperplano de separación en estos espacios transformados, normalmente de muy alta dimensión, se hará de forma implícita utilizando las denominadas funciones kernel. 

Mientras la mayoría de los métodos de aprendizaje se centran en minimizar los errores cometidos por el modelo generado a partir de los ejemplos de entrenamiento (error empírico), el sesgo inductivo asociado a los SVMs radica en la minimización del denominado riesgo estructural. La idea es seleccionar un hiperplano de separación que equidista de los ejemplos más cercanos de cada clase para, de esta forma, conseguir lo que se denomina un margen máximo a cada lado del hiperplano. Además, a la hora de definir el hiperplano, sólo se consideran los ejemplos de entrenamiento de cada clase que caen justo en la frontera de dichos márgenes. Estos ejemplos reciben el nombre de vectores soporte. Desde un punto de vista práctico, el hiperplano separador de margen máximo ha demostrado tener una buena capacidad de generalización, evitando en gran medida el problema del sobreajuste a los ejemplos de entrenamiento.	

Desde un punto de vista algorítmico, el problema de optimización del margen geométrico representa un problema de optimización cuadrático con restricciones lineales que puede ser resuelto mediante técnicas estándar de programación cuadrática. La propiedad de convexidad exigida para su resolución garantizan una solución única, en contraste con la no unicidad de la solución producida por una red neuronal artificial entrenada con un mismo conjunto de ejemplos.

%Dado el carácter introductorio de este tutorial, los contenidos del mismo sólo abarcan una pequeña parcela del extenso campo relacionado con las máquinas vectores soporte. Por ejemplo, el problema de clasificación sólo se describirá para el caso de clases binarias. Concretamente, en la sección 2 se abordará el problema de clasificación binaria para ejemplos perfectamente separables mediante lo que se conoce como SVMs de "margen duro" (hard margen). Dado que en la práctica es normal que los ejemplos de entrenamiento puedan contener ruido, la sección 3 se dedica abordar el problema de clasificación binaria para ejemplos cuasi-separables linealmente, mediante lo que se denomina SVMs de "margen blando" (soft margen). La  sección 4 culmina el problema de clasificación binaria tratando el caso de la clasificación de ejemplos no separables lienalmente mediante lo  que se conoce como SVM kernelizadas. Seguidamente, la sección 5 aborda  el problema  de regresión mediante lo que se cono ce como SVR (del inglés Support Vector Regression machines). En esta sección, se  aborda tanto el caso de regresión lineal como el caso de regresión no lineal. En la sección 6 se describ e algunos de los paquetes software de uso libre más relevante dedicados a la implementación de SVMs. Pueden ser un buen punto  de comienzo para que el lector se familiarice, desde un punto de vista práctico, con este paradigma de aprendizaje. Finalmente, la sección 7 corresponde a un anexo dedicado a formular, de forma resumida, aquellos resultados de la teoría de la optimización necesarios para solucionar los diferentes  problemas de optimización que surgen como consecuencia de ab ordar los problemas de clasificación y de regresión mediante SVMs. 

\section{SVM 	para 	clasificación 	binaria 	de 	ejemplos 	separables 	linealmente}
 		 		 
 		 		 
Dado un conjunto separable de ejemplos $S= \{(\bm{x}_1,y_1),...,(\bm{x}_n,y_n)\}$, donde $\bm{x}\in {\rm I\!R}^d$ e $y_i\in \{+1,-1\}$, se puede definir un hiperplano de separación (ver Figura \ref{Figura3_01}) como una función lineal que es capaz de separar dicho conjunto sin error:

\begin{equation}
	D(x) = (\omega_1x_1+...+\omega_dx_d) + b  = <\bm{\omega},\bm{x}>+b
\end{equation} 

donde $\bm{\omega}$ y $b$ son coeficientes reales.

\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.8\linewidth]{capitulo_03/figuras_dir/diagrama1.png}
	\caption{Hiperplanos de	separación en un espacio bidimensional de un conjunto de ejemplos separables en dos clases: (a) ejemplo de hiperplano de separación (b) otros ejemplos de hiperplanos de separación, de entre los infinitos posibles.} 	
	\label{Figura3_01}
\end{figure}

 El hiperplano de separación cumplirá las siguientes restricciones para todo $\bm{x}_i$ del conjunto de ejemplos: 

\begin{align}
	& <\bm{\omega},\bm{x}_i> + b \ge 0 ~~\textrm{si}~~ y_i=+1, ~~ i=1,...,n  \nonumber \\ 
	& <\bm{\omega},\bm{x}_i> + b \le 0 ~~\textrm{si}~~ y_i=-1, ~~ i=1,...,n   
\end{align}

O escrito de forma más compacta de la siguiente forma:

\begin{equation} \label{3}
 y_i(<\bm{\omega},\bm{x}_i> + b) \ge 0, ~~~ i=1,...,n
\end{equation}

Es decir,
\begin{equation}\label{4}
 y_iD(\bm{x}_i) \ge 0, ~~~ i=1,...,n
\end{equation}


Tal y como se puede deducir fácilmente de la Figura \ref{Figura3_01}b, el hiperplano que permite separar los ejemplos no es único, es decir, existen infinitos hiperplanos separables representados por todos aquellos hiperplanos que son capaces de cumplir las restricciones impuestas por cualquiera de las expresiones equivalentes (\ref{3}-\ref{4}). Surge entonces la pregunta sobre si es posible establecer algún criterio adicional que permita definir un hiperplano de separación óptimo. Para ello, primero, se define el concepto de margen de un hiperplano de separación, denotado por $\tau$, como la mínima distancia entre dicho hiperplano y el ejemplo más cercano de cualquiera de las dos clases (ver Figura \ref{Figura3_02}a). A partir de  esta definición, un hiperplano de separación se denominará óptimo si su margen es de tamaño máximo (Figura \ref{Figura3_02}b). Una propiedad inmediata de la definición de hiperplano de separación óptimo es que éste equidista del ejemplo más cercano de cada clase. La demostración de esta propiedad se puede hacer fácilmente por reducción al absurdo. 
Supongamos que la distancia del hiperplano óptimo al ejemplo más cercano de la clase $+1$ fuese menor que la correspondiente al ejemplo más cercano de la clase $-1$. 

Esto significaría que se puede alejar el hiperplano del ejemplo de la clase $+1$ una distancia tal que la distancia del hiperplano a dicho ejemplo sea mayor que antes y, a su vez, siga siendo menor que la distancia al ejemplo más cercano de la clase $-1$. Se llega así al absurdo de poder aumentar el tamaño del margen cuando, de partida, habíamos supuesto que éste era máximo (hiperplano óptimo). Se aplica un razonamiento similar en el caso de suponer que la distancia del hiperplano óptimo al ejemplo más cercano de la clase $-1$ fuese menor que la correspondiente al ejemplo más cercano de la clase $+1$. 

\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.8\linewidth]{capitulo_03/figuras_dir/diagrama2.png}
	\caption{Margen de un hiperplano de separación: (a) hiperplano de separación no-óptimo y su 		margen asociado (no máximo) (b) hiperplano de separación óptimo y su margen asociado (máximo).} 	
	\label{Figura3_02}
\end{figure}

Por geometría, se sabe que la distancia entre un hiperplano de separación $D(x)$ y un ejemplo $x'$ viene dada por 

\begin{equation}\label{5}
	\frac{|D(x')|}{||\bm{\omega}||}
\end{equation}


siendo $|\cdot|$ el operador valor absoluto, $||\cdot||$ el operador norma de un vector y $\bm{\omega}$ el vector que, junto con el parámetro $b$, define el hiperplano $D(x)$ y que, además, tiene la propiedad de ser perpendicular al hiperplano considerado. Haciendo uso de la expresiones \ref{4} y \ref{5}, todos los ejemplos de entrenamiento cumplirán que:

\begin{equation}\label{6}
\frac{y_iD(\bm{x}_i)}{||\bm{\omega}||} \ge \tau, ~~~ i=1,...,n
\end{equation}


De la expresión anterior, se deduce que encontrar el hiperplano óptimo es equivalente a encontrar el valor de $\omega$ que maximiza el margen. Sin embargo, existen infinitas soluciones que difieren solo en la escala  de $\omega$ . Así, por ejemplo, todas las funciones lineales $\lambda$ $(<\bm{\omega},\bm{x}_i> + b)$ , con $\lambda\in R$ ,  representan el mismo hiperplano. Para limitar, por tanto, el  número de soluciones a una sola, y teniendo en cuenta que \ref{6} se puede expresar también como  

\begin{equation}\label{7}
\frac{y_iD(\bm{x}_i)}{||\bm{\omega}||} \ge \tau, ~~~ i=1,...,n
\end{equation}

la escala del producto de $\tau$ y la norma de $\omega$ se fija, de forma arbitraria, a la unidad, es decir 

\begin{equation}\label{8}
	\tau||\bm{\omega}||=1
\end{equation}


Llegando a la conclusión final de que aumentar el margen es equivalente a disminuir la norma de $\omega$, ya que la expresión anterior se puede expresar como 

\begin{equation}\label{9}
\tau=\frac{1}{||\bm{\omega}||}
\end{equation}

\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.5\linewidth]{capitulo_03/figuras_dir/diagrama3.png}
	\caption{La 		distancia 		de 		cualquier 		ejemplo, 		$\bm{x}_i$		, 	 al 		hiperplano 		de 		separación 		óptimo  		viene 		dada 		por 		$|D(\bm{x}_i)|/||\bm{\omega}||$ . 		En 		particular, 		si 		dicho  		ejemplo 		pertenece 		al 	 conjunto 		de 		vectores 		soporte  		(identificados 		por 		siluetas 		sólidas), 		la 		distancia 		a 		dicho 		hiperplano 		será  		siempre 		$1/||\omega||$. 		Además, 		los 		vectores 		soporte 		aplicados 		a 		la 		función 		de 	 decisión 		siempre 		cumplen 		que 		$|D(\bm{x})|=1$.} 	 
		\label{Figura3_03}
\end{figure}



Por tanto, de acuerdo a su definición, un hiperplano de separación óptimo (ver Figura \ref{Figura3_03}) será aquel que posee un margen máximo y, por tanto, un valor mínimo de $||\omega||$ y, además, está sujeto a la restricción dada por \ref{7}, junto con el criterio expresado por \ref{8}, es decir, el resultado obtenido en \ref{10}.

\begin{equation}\label{10}
y_iD(\bm{x}_i) \ge 1, ~~~ i=1,...,n
\end{equation}

o lo que es lo mismo

\begin{equation}\label{11}
y_iD(<\bm{\omega},\bm{x}_i> + b) \ge 1, ~~~ i=1,...,n
\end{equation}

El concepto de margen máximo está relacionado directamente con la capacidad de generalización del hiperplano de separación, de tal forma que, a mayor margen, mayor distancia de separación existirá entre las dos clases. Los ejemplos que están situados a ambos lados del hiperplano óptimo y que definen el margen o, lo que es lo mismo, aquellos para los que la restricción \ref{11} es una igualdad, reciben el nombre de vectores soporte (ver Figura \ref{Figura3_03}). Puesto que estos ejemplos son los más cercanos al hiperplano de separación, serán los más difíciles de clasificar y, por tanto, deberían ser los únicos ejemplos a considerar a la hora de construir dicho hiperplano. De hecho, se demostrará más adelante, en esta misma sección, que el hiperplano de separación óptimo se define sólo a partir de estos vectores.

La búsqueda del hiperplano óptimo para el caso separable puede ser formalizado como el problema de encontrar el valor de $\omega$ y $b$ que minimiza el funcional $f ( \omega ) = || \omega ||$ sujeto a las restricciones \ref{11}, o de forma equivalente 
\begin{align}
& \min~~ f(\omega) =\frac{1}{2}||\omega||^2 = \frac{1}{2}<\omega,\omega> \nonumber \\
& \textrm{s.a.}~~ y_i(<\bm{\omega},\bm{x}_i> + b)-1\ge 0,  ~~~ i=1,...,n \label{12}
\end{align}

Este problema de optimización con restricciones corresponde a un problema de programación cuadrático  y es abordable mediante la teoría de la optimización. Dicha teoría establece que un problema de optimización, denominado primal, tiene una  forma dual si la función a  optimizar y las restricciones son funciones  estrictamente convexas. En estas circunstancias, resolver el problema dual permite obtener la solución del problema primal.

Así, puede demostrarse que el problema de optimización dado por \ref{12} satisface el criterio de convexidad y, por tanto, tiene un problema dual asociado que quedaría:

\begin{equation}
 \max \sum_{i=1}^{n} \alpha_i -\frac{1}{2} \sum_{i,j=1}^{n} \alpha_i \alpha_j y_i y_j <\bm{x}_i,\bm{x}_j>
\end{equation} 

\begin{equation}
s.a ~~ \sum_{i=1}^{n}\alpha_iy_i=0
\end{equation} 

\begin{equation}
\alpha_i > 0, ~~~~~~~~~ i=1,...,n
\end{equation} 


\section{SVM Caso Lineal No Separable}

En los problemas reales encontrar un conjunto con dos clases totalmente separables es escasamente probable, entre otras cosas por la existencia de ruido en los datos. La idea para tratar con este tipo de casos con ruido es introducir un conjunto de variables reales y positivas, variables artificiales, $\xi_i , i = 1,...,n$, de forma que permitan algunos ejemplos no separables, es decir:

\begin{equation}
y_i (<\bm{\omega}, \bm{x}_i > + b) \ge 1-\xi_i, ~~~~  \xi_i  \ge 0  , ~~~ i = 1,...,n
\end{equation}

De acuerdo con la expresión anterior los ejemplos con variable artificial nula corresponden a ejemplos separables, mientras que los que tengan asociada una variable artificial positiva son los llamados ruido, mal clasificados. La función a optimizar en este caso debe incluir estas variables artificiales de forma que controle el error en la clasificación.


De esta forma el nuevo problema de optimización a resolver nos quedaría:
\begin{equation}
\min \frac{1}{2} ||\bm{\omega}||^2 + C \sum_{i=1}^{n} \xi_i
\end{equation} 

\begin{equation}
s.a ~~ y_i (<\bm{\omega}, \bm{x}_i > + b) - 1+ \xi_i\ge 0 ~~~~ i=1,...,n
\end{equation} 

\begin{equation}
\xi_i\ge 0, ~~~~~~~~~ i=1,...,n
\end{equation} 

donde $C$ es una constante positiva a determinar de la que puede depender el clasificador.


El hiperplano definido tras resolver este problema se denomina hiperplano de separación de margen blando o \textit{soft margin}. En el caso perfectamente separable el hiperplano obtenido se le denomina hiperplano de separación duro o \textit{hard margin}. 

\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.5\linewidth]{capitulo_03/figuras_dir/Selection_282.png}
	\caption{Ejemplo de problema no separable linealmente.} 	 
	\label{Figura3_04}
\end{figure}

El problema dual asociado a este nuevo problema quedaría:

\begin{equation}
\max \sum_{i=1}^{n} \alpha_i -\frac{1}{2} \sum_{i,j=1}^{n} \alpha_i \alpha_j y_i y_j <\bm{x}_i,\bm{x}_j>
\end{equation} 

\begin{equation}
s.a ~~ \sum_{i=1}^{n}\alpha_iy_i=0
\end{equation} 

\begin{equation}
0 \le \alpha_i \le C, ~~~~~~~~~ i=1,...,n
\end{equation} 


\section{SVM Caso No Lineal}

Es muy común en los problemas reales clasificar un conjunto donde los ejemplos no suelen ser separables. Aunque podemos utilizar el enfoque descrito en la sección anterior, sería deseable poder usar estrategias no lineales. Para resolver la clasificación mediante SVM de este tipo de conjuntos, se realiza la inmersión no lineal de nuestro conjunto en un espacio de dimensión mayor donde sí sean separables, y en este nuevo espacio buscar el hiperplano de separación. Este espacio se denomina
espacio de características.

Sea $\Phi : X \rightarrow F$ la función que hace corresponder a cada punto de entrada x un punto en el espacio de características $F$. Lo que pretendemos es encontrar el hiperplano de separación en este nuevo espacio $F$. Este hiperplano en el espacio de características se transforma en una función no lineal que separa nuestro conjunto en el espacio original de entradas.

De esta forma, la función de decisión en el espacio de características viene dada por:

\begin{equation}
D (x) = <\bm{\omega}, \Phi (\bm{x})_i> + b
\end{equation}

El problema primal asociado a este caso es similar al descrito previamente con la salvedad de que el hiperplano de separación se determina en el espacio de características, es decir:

\begin{equation}\label{p2}
\min \frac{1}{2} ||\bm{\omega}||^2 + C \sum_{i=1}^{n} \xi_i
\end{equation} 

\begin{equation}
s.a ~~ y_i (<\bm{\omega}, \phi(\bm{x}_i)) - 1+ \xi_i\ge 0 ~~~~ i=1,...,n
\end{equation} 

\begin{equation}
\xi_i\ge 0, ~~~~~~~~~ i=1,...,n
\end{equation} 


\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.8\linewidth]{capitulo_03/figuras_dir/Selection_283.png}
	\caption{Ejemplo gráfico de una posible función $\Phi$.} 	 
	\label{Figura3_05}
\end{figure}


La complejidad del problema \ref{p2} depende de la dimensión del espacio de características, que suele ser alta. Por ello se considera el problema dual asociado, cuya complejidad depende del número de ejemplos.

En este caso el dual quedaría:

\begin{equation}
\max \sum_{i=1}^{n} \alpha_i -\frac{1}{2} \sum_{i,j=1}^{n} \alpha_i \alpha_j y_i y_j <\Phi(\bm{x}_i),\Phi(\bm{x}_j)>
\end{equation} 

\begin{equation}
s.a ~~ \sum_{i=1}^{n}\alpha_iy_i=0
\end{equation} 

\begin{equation}
0 \le \alpha_i \le C, ~~~~~~~~~ i=1,...,n
\end{equation} 

En el problema anterior se observa que no es necesario conocer las componentes $\phi$ i de la función de inversión, sino sólo los productos escalares $<\Phi (x_i) , \Phi (x_j)>$. Para resolver este tipo de problemas se usa el \textit{truco del kernel}, donde:

\begin{equation}
K (x_i , x_j ) = <\Phi (x_i) , \Phi (x_j)>
\end{equation}

De esta forma podemos construir una función de decisión en el espacio original sin usar explícitamente la función de inversión $\Phi$. 

\subsection{Ejemplos 	de 	funciones 	kernel} \label{funciones_kernel}

Se presentan aquí algunos ejemplos de funciones kernel:

\begin{itemize}
	\item Kernel lineal:
	\begin{equation}
	K(\bm{x},\bm{x}') = <\bm{x},\bm{x}'>
	\end{equation}
	\item Kernel polinómico de grado-$p$:
	\begin{equation}
	K_p(\bm{x},\bm{x}') = [\gamma<\bm{x},\bm{x}'>+\tau]^p\cdot p
	\end{equation}
	\item Kernel RBF: Radial Basis Function (gausiano):
	\begin{equation}
	K(\bm{x},\bm{x}') = \exp\left(-\gamma||\bm{x}-\bm{x}'||^2\right), ~~\gamma>0
	\end{equation}
	\item Kernel sigmoidal:
	\begin{equation}
	K(\bm{x},\bm{x}') = \tanh\left(\gamma<\bm{x},\bm{x}'>+\tau\right)
	\end{equation}
\end{itemize}

A los parámetros $\gamma$, $\tau$ y $p$ se les denomina parámetros del kernel y más adelante en la memoria estos parámetros se denominan:

\begin{itemize}
	\item $\tau$ se representa a través de \textit{coef0} en las aplicaciones.
	\item $\gamma$ se representa por \textit{gamma} en las aplicaciones.
	\item $p$ se representa por \textit{degree} en las aplicaciones.
\end{itemize}