WHAT IS THIS

  This file contains information about the PT Designer project and
  files on this CD. The official project website including FAQ and
  simple tutorials is

  http://tastyfish.cz/ptdesigner.html,

  the project source code can be found at GitHub (or this CD)

  https://github.com/drummyfish/proctextures.

________________________________________________________________________________

DIRECTORIES

  - binaries            
    - texturemaker      GUI tool (Texturemaker) Windows stand-alone executable
      -test textures    example XML textures that can be loaded with Texturemaker
      -test grammars    grammar files used by some example textures
  - code documentation   
    - html              library code Doxygen HTML documentation
  - project             project source codes
    - lib               precompiled library binaries (for Windows and Linux)
    - sources           library C/C++ sources
    - texturemaker      GUI tool (Texturemaker) QT sources
  - posters             project posters
  - text                thesis text and its sources
    - latex             Latex thesis sources

________________________________________________________________________________

COMPILING THE PROJECT

  To compile the library, run 'make lib' command in the project directory. This
  compiles the sources with gcc and c++ and packs them into .a library file.
  This will easily work with Linux, with Windows it is required to have MinGW
  and Make for Windows installed.

  To install the library, just put the header files from project/sources and
  libptdesigner.a from project/lib where they belong. On Windows this can be for
  example C:/MinGW/include for headers and C:/MinGW/lib for the library file.

  To compile the GUI tool (Texturemaker) run QT environment and open the file
  project/texturemaker/texturemaker.pro, then just build. To make the library
  link successfully, the compiled libptdesigner.a file must be placed in
  project/lib/{linux64|win32} (but they're already there).

________________________________________________________________________________

USAGE

  The GUI tool (Texturemaker) can be run from the binaries directory on Windows
  operating system. On Linux, it's necessary to compile the QT sources in order
  to run it.

  There are blocks in the bottom of the window that can be drag-and-dropped onto
  the canvas. The blocks can be then connected using the left mouse button. Right
  mouse button cancels the existing connection between the blocks. Double click the
  block to edit its parameters. You can create the texture description graph this
  way and execute its computation by clicking the execute button in the toolbar
  menu.

  There are some example XML textures in the test textures subfolder that can be
  loaded to get some idea of how it works.

  For more details see the official project website (mentioned above).

________________________________________________________________________________

Miloslav ��, 2014