var searchData=
[
  ['kd_5ftree_5fdestroy',['kd_tree_destroy',['../kdtree_8c.html#ab72b8a32f1dad5a8d31bbfc5838c3fd5',1,'kd_tree_destroy(t_kd_tree *tree):&#160;kdtree.c'],['../kdtree_8h.html#ab72b8a32f1dad5a8d31bbfc5838c3fd5',1,'kd_tree_destroy(t_kd_tree *tree):&#160;kdtree.c']]],
  ['kd_5ftree_5ffind_5fnearest_5fneighbour',['kd_tree_find_nearest_neighbour',['../kdtree_8c.html#a74cee134fc74b7f1acc5dd9409f83a36',1,'kd_tree_find_nearest_neighbour(t_kd_tree *tree, int x, int y, int *nearest_x, int *nearest_y, int space_width, int space_height):&#160;kdtree.c'],['../kdtree_8h.html#a74cee134fc74b7f1acc5dd9409f83a36',1,'kd_tree_find_nearest_neighbour(t_kd_tree *tree, int x, int y, int *nearest_x, int *nearest_y, int space_width, int space_height):&#160;kdtree.c']]],
  ['kd_5ftree_5finit',['kd_tree_init',['../kdtree_8c.html#ab9a103e74c7c9416c38ff5d245bbb71f',1,'kd_tree_init(t_kd_tree *tree, int points[][2], int points_length):&#160;kdtree.c'],['../kdtree_8h.html#ab9a103e74c7c9416c38ff5d245bbb71f',1,'kd_tree_init(t_kd_tree *tree, int points[][2], int points_length):&#160;kdtree.c']]]
];
