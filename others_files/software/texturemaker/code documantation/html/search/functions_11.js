var searchData=
[
  ['transform_5fcoordination',['transform_coordination',['../general_8c.html#a48864b12a404f4038c451abf302f69bc',1,'transform_coordination(int coordination, int limit):&#160;general.c'],['../general_8h.html#a48864b12a404f4038c451abf302f69bc',1,'transform_coordination(int coordination, int limit):&#160;general.c']]],
  ['two_5fpoints_5fto_5fangle',['two_points_to_angle',['../general_8c.html#aac440bf89c33d72dfb30c1c1988028be',1,'two_points_to_angle(int x1, int y1, int x2, int y2):&#160;general.c'],['../general_8h.html#aac440bf89c33d72dfb30c1c1988028be',1,'two_points_to_angle(int x1, int y1, int x2, int y2):&#160;general.c']]],
  ['type',['type',['../classrapidxml_1_1xml__node.html#a2c6a4315b98bcfa2e04fed3fa1b22c36',1,'rapidxml::xml_node::type() const '],['../classrapidxml_1_1xml__node.html#a499bbc9300c1b06821d5c08b24164c68',1,'rapidxml::xml_node::type(node_type type)']]]
];
