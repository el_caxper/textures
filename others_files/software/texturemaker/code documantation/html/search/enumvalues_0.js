var searchData=
[
  ['detection_5fboth',['DETECTION_BOTH',['../proctextures_8h.html#a417d335403bae30f4f0718019b9cf514aecfc6c52074d07657239475ddc7c7354',1,'proctextures.h']]],
  ['detection_5fhorizontal',['DETECTION_HORIZONTAL',['../proctextures_8h.html#a417d335403bae30f4f0718019b9cf514af26a4a7160c431aabf6796b4e431461d',1,'proctextures.h']]],
  ['detection_5fvertical',['DETECTION_VERTICAL',['../proctextures_8h.html#a417d335403bae30f4f0718019b9cf514ad850c800ae8a0e0035039e9978d8a795',1,'proctextures.h']]],
  ['direction_5fdiagonal_5fld_5fru',['DIRECTION_DIAGONAL_LD_RU',['../proctextures_8h.html#aec4e3894aa763e81148b2241ff730790a7c2090d5c08b27c46cdaa7f4c5ea0c27',1,'proctextures.h']]],
  ['direction_5fdiagonal_5flu_5frd',['DIRECTION_DIAGONAL_LU_RD',['../proctextures_8h.html#aec4e3894aa763e81148b2241ff730790aa3000f137958982ae2f3e87f76da7dde',1,'proctextures.h']]],
  ['direction_5fhorizontal',['DIRECTION_HORIZONTAL',['../proctextures_8h.html#aec4e3894aa763e81148b2241ff730790ab78abbbaeccca0b38366077bfa556de5',1,'proctextures.h']]],
  ['direction_5fvertical',['DIRECTION_VERTICAL',['../proctextures_8h.html#aec4e3894aa763e81148b2241ff730790ae838b1f33cb3fecd4e7c3957916e2fd9',1,'proctextures.h']]],
  ['dithering_5ferror_5fpropagation',['DITHERING_ERROR_PROPAGATION',['../proctextures_8h.html#a616f44d7a89d67c5870059e735c4fed1ac56f72f303abf62e8ffcf277956a5f8b',1,'proctextures.h']]],
  ['dithering_5fordered',['DITHERING_ORDERED',['../proctextures_8h.html#a616f44d7a89d67c5870059e735c4fed1a68b701385a2a4b100ac5a18c39d59237',1,'proctextures.h']]],
  ['dithering_5frandom',['DITHERING_RANDOM',['../proctextures_8h.html#a616f44d7a89d67c5870059e735c4fed1ada862ed04539b7dafa2d78e53b6a92d1',1,'proctextures.h']]],
  ['dithering_5fthreshold',['DITHERING_THRESHOLD',['../proctextures_8h.html#a616f44d7a89d67c5870059e735c4fed1ae817ea4af7d65db6925d55a23341cffb',1,'proctextures.h']]]
];
