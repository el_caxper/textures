var searchData=
[
  ['radians_5fto_5fdegrees',['radians_to_degrees',['../general_8c.html#a6a7a727552b98b43cd1893c96b30b939',1,'radians_to_degrees(double radians):&#160;general.c'],['../general_8h.html#a6a7a727552b98b43cd1893c96b30b939',1,'radians_to_degrees(double radians):&#160;general.c']]],
  ['remove_5fall_5fattributes',['remove_all_attributes',['../classrapidxml_1_1xml__node.html#aa8d5d9484aa1eb5ff1841a073c84c1aa',1,'rapidxml::xml_node']]],
  ['remove_5fall_5fnodes',['remove_all_nodes',['../classrapidxml_1_1xml__node.html#a95735358b079ae0adcfbbac69aa1fbc3',1,'rapidxml::xml_node']]],
  ['remove_5fattribute',['remove_attribute',['../classrapidxml_1_1xml__node.html#a6f97b1b4f46a94a4587915df3c0c6b57',1,'rapidxml::xml_node']]],
  ['remove_5fblock',['remove_block',['../classpt__design_1_1c__texture__graph.html#a035b9a6ff6dee010ca0d43efe17c6357',1,'pt_design::c_texture_graph']]],
  ['remove_5ffirst_5fattribute',['remove_first_attribute',['../classrapidxml_1_1xml__node.html#aa95192d2a165cca16c551ed2a2a06aec',1,'rapidxml::xml_node']]],
  ['remove_5ffirst_5fnode',['remove_first_node',['../classrapidxml_1_1xml__node.html#a62bf7b276cf7a651a3337f5e0a0ef6ac',1,'rapidxml::xml_node']]],
  ['remove_5flast_5fattribute',['remove_last_attribute',['../classrapidxml_1_1xml__node.html#a1781a2cbedc9a51d609ad5b528125635',1,'rapidxml::xml_node']]],
  ['remove_5flast_5fnode',['remove_last_node',['../classrapidxml_1_1xml__node.html#a9182512e948ec451a83f116cce7c7674',1,'rapidxml::xml_node']]],
  ['remove_5fnode',['remove_node',['../classrapidxml_1_1xml__node.html#a98289923eb9e8889418a9eb0207ea35c',1,'rapidxml::xml_node']]],
  ['rgb_5fto_5fhsl',['rgb_to_hsl',['../general_8c.html#a99a362adb9c9cd91088f8339eda3ee3a',1,'rgb_to_hsl(unsigned char red, unsigned char green, unsigned char blue, double *hue, double *saturation, double *lightness):&#160;general.c'],['../general_8h.html#a99a362adb9c9cd91088f8339eda3ee3a',1,'rgb_to_hsl(unsigned char red, unsigned char green, unsigned char blue, double *hue, double *saturation, double *lightness):&#160;general.c']]],
  ['rock_5fpaper_5fscissors',['rock_paper_scissors',['../general_8c.html#a5bb5683b6093afb05bd3646d9648288a',1,'rock_paper_scissors(unsigned int options, unsigned int player1, unsigned int player2):&#160;general.c'],['../general_8h.html#a5bb5683b6093afb05bd3646d9648288a',1,'rock_paper_scissors(unsigned int options, unsigned int player1, unsigned int player2):&#160;general.c']]],
  ['round_5fto_5fchar',['round_to_char',['../general_8c.html#af495a3a06b4bba387dacb6fb85f0e45e',1,'round_to_char(int value):&#160;general.c'],['../general_8h.html#af495a3a06b4bba387dacb6fb85f0e45e',1,'round_to_char(int value):&#160;general.c']]]
];
