from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import portrait
from PyPDF2.pdf import PdfFileWriter, PdfFileReader

# a reader
# reader = PdfFileReader(open("fpath", 'rb'))

# a writer
writer = PdfFileWriter()
writer.addBlankPage(50,50)
# writer.addJS("this.print({bUI:true,bSilent:false,bShrinkToFit:true});")
# writer.write("Hola")
outfp = open("outpath", 'wb')
writer.write(outfp)

# Ejemplo usando https://github.com/mstamy2/PyPDF2
# y reportlab: https://www.reportlab.com/docs/reportlab-userguide.pdf