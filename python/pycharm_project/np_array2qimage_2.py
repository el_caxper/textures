from skimage import img_as_ubyte
from skimage.io import imread
from skimage import data_dir
from skimage.transform import radon, rescale

from PySide2.QtWidgets import QMainWindow, QLabel, QSizePolicy, QApplication
from PySide2.QtGui import QPixmap, QImage
from PySide2.QtCore import Qt

import sys

from skimage.io import imread, imsave

from skimage.color import rgb2gray, rgb2grey
from skimage.color import gray2rgb

import numpy as np

from PIL import Image
class TransformImage(QMainWindow):

 def __init__(self):
     super().__init__()
     self.initUI()

 def initUI(self):
     self.setGeometry(10,10,640, 400)

     pixmap_label = QLabel()
     pixmap_label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
     pixmap_label.resize(640,400)
     pixmap_label.setAlignment(Qt.AlignCenter)

     image = imread("/home/kaiser/index.png", as_gray=True)
     #image = rescale(image, scale=0.4, mode='reflect', multichannel=False)
     imsave("/home/kaiser/phantom.png", image)
     image = np.uint8((image - image.min()) / image.ptp() * 255.0)
     h, w = image.shape[:2]
     print(len(image.shape))
     qimg_format = QImage.Format_RGB888 if len(image.shape) == 3 else QImage.Format_Grayscale8
     qimage = QImage(image.data, image.shape[1], image.shape[0], image.strides[0], qimg_format)
     qimage.save("/home/kaiser/qphantom.png")


     pixmap = QPixmap(qimage)
     pixmap = pixmap.scaled(h,w, Qt.KeepAspectRatio)
     pixmap_label.setPixmap(pixmap)

     self.setCentralWidget(pixmap_label)
     self.show()



def main():
  app = QApplication(sys.argv)
  win = TransformImage()
  sys.exit(app.exec_())



if __name__=="__main__":
  main()
