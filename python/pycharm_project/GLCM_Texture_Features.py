from Sample import Sample as Sampledb
from Sample import Image as Imagedb
from Sample import Feature as Featuredb

import peewee

from os import listdir
from os.path import isfile, join
import os.path
import  numpy as np

from skimage.io import imread
from skimage import img_as_ubyte

from skimage.feature import greycomatrix, greycoprops
import matplotlib.pyplot as plt


class Features:
    def __init__(self, parent=None):
        try:
            from Sample import db
            db.init('/home/kaiser/PycharmProjects/textures/dataset/bark1/Sample.db')
            self.Sample = Sampledb()
            self.Image = Imagedb()
            self.Feature = Featuredb()
            self.Sample.create_table()
            self.Image.create_table()
            self.Feature.create_table()
        except peewee.OperationalError:
            print("tables already exist!")

        self.imagedb, self.imagedb_created = self.Image.get_or_create(
            image_name='T01_02.jpg')

        self.m_image = imread("/home/kaiser/PycharmProjects/textures/dataset/bark1/T01_02.jpg", as_gray=True)
        self.m_image_size = self.m_image.shape
        print("x real: {}, y real: {}".format(self.m_image_size[0], self.m_image_size[1]))
        self.m_image = img_as_ubyte(self.m_image)

        self.coords = []
        self.coords_patches = []

        if not self.imagedb_created:
            self.mysamples_db = self.imagedb.samples

            if len(self.mysamples_db) > 0:

                for i in self.mysamples_db:
                    print("IMAGE: {0}".format('T01_02.jpg'))
                    print("label: {}".format(i.label))
                    print("x: {}".format(i.x))
                    print("y: {}".format(i.y))
                    print("size: {}".format(i.size))
                    print("positive: {}".format(i.positive))
                    self.PATCH_SIZE = i.size
                    self.coords.append((i.y,i.x))
                    print("x coords: {}, y coords: {}".format(i.y, i.x))
                    self.features = i.features
                    self.actual_sample=  i

                    if len(self.features) > 0:

                        for feature in self.features:
                            print("correlation: {}".format(feature.correlation))
                            print("contrast: {}".format(feature.contrast))
                            print("dissimilarity: {}".format(feature.dissimilarity))
                            print("homogeneity: {}".format(feature.homogeneity))
                            print("ASM: {}".format(feature.ASM))
                            print("energy: {}".format(feature.energy))
                            feature.delete().execute()


                    #self.actual_sample.features.on_delete()




        db_images = self.Image.select()
        images_db_list = [image.image_name for image in db_images]
        for image_db in images_db_list:
            print("{0}".format(self.imagedb.image_name))

        for loc in self.coords:
            self.coords_patches.append(self.m_image[loc[0]:loc[0] + self.PATCH_SIZE,
                                 loc[1]:loc[1] + self.PATCH_SIZE])

        # compute some GLCM properties each patch
        diss = []
        corr = []
        cont = []
        homo = []
        asm  = []
        ener = []
        for patch in (self.coords_patches ):
            glcm = greycomatrix(patch, [5], [0], 256, symmetric=True, normed=True)
            cont.append(greycoprops(glcm, 'contrast')[0, 0])
            diss.append(greycoprops(glcm, 'dissimilarity')[0, 0])
            homo.append(greycoprops(glcm, 'homogeneity')[0, 0])
            asm.append(greycoprops(glcm, 'ASM')[0, 0])
            ener.append(greycoprops(glcm, 'energy')[0, 0])
            corr.append(greycoprops(glcm, 'correlation')[0, 0])

            self.Feature.get_or_create(sample=self.actual_sample,
                                       contrast=greycoprops(glcm, 'contrast')[0, 0],
                                       dissimilarity=greycoprops(glcm, 'dissimilarity')[0, 0],
                                       homogeneity=greycoprops(glcm, 'homogeneity')[0, 0],
                                       ASM=greycoprops(glcm, 'ASM')[0, 0],
                                       energy=greycoprops(glcm, 'energy')[0, 0],
                                       correlation= greycoprops(glcm, 'correlation')[0, 0])

        # create the figure
        fig = plt.figure(figsize=(8, 8))

        # display original image with locations of patches
        ax = fig.add_subplot(3, 2, 1)
        ax.imshow(self.m_image, cmap=plt.cm.gray, interpolation='nearest',
                  vmin=0, vmax=255)
        for (y, x) in self.coords:
            ax.plot(x , y , 'gs')
        ax.set_xlabel('Original Image')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.axis('image')

        # for each patch, plot (dissimilarity, correlation)
        ax = fig.add_subplot(3, 2, 2)
        ax.plot(diss[:len(self.coords_patches)], corr[:len(self.coords_patches)], 'go',
                label='Grass')
        # ax.plot(xs[len(self.coords_patches):], ys[len(self.coords_patches):], 'bo',
        #         label='Sky')
        ax.set_xlabel('GLCM Dissimilarity')
        ax.set_ylabel('GLCM Correlation')
        ax.legend()

        # display the image patches
        for i, patch in enumerate(self.coords_patches):
            ax = fig.add_subplot(3, len(self.coords_patches), len(self.coords_patches) * 1 + i + 1)
            ax.imshow(patch, cmap=plt.cm.gray, interpolation='nearest',
                      vmin=0, vmax=255)
            ax.set_xlabel('Grass %d' % (i + 1))


        # display the patches and plot
        fig.suptitle('Grey level co-occurrence matrix features', fontsize=14)
        plt.show()




if __name__=="__main__":
  Features()