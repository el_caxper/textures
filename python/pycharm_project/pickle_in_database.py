from Pickle_DDBB import Pickle as Pickledb

import peewee

from os import listdir
from os.path import isfile, join
import os.path
import  numpy as np

from skimage.io import imread
from skimage import img_as_ubyte

from skimage.feature import greycomatrix, greycoprops
import matplotlib.pyplot as plt

from sklearn import svm, metrics

import pickle

from sklearn.datasets import make_classification
import numpy as np
from imblearn.ensemble import BalancedBaggingClassifier
from sklearn.svm import SVC

class Pickle_database:
    def __init__(self, parent=None):
        try:
            from Pickle_DDBB import db
            db.init('/home/kaiser/PycharmProjects/textures/python/pycharm_project/Pickle.db')
            self.Pickledb = Pickledb()
            self.Pickledb.create_table()
        except peewee.OperationalError:
            print("tables already exist!")

        X = np.array([[-1, -1], [-2, -1], [1, 1], [2, 1]])
        y = np.array([1, 1, 2, 2])

        self.balanced_bagging = SVC()

        pdata = pickle.dumps(self.balanced_bagging, pickle.HIGHEST_PROTOCOL)

        try:
            self.pickledb, self.pickledb_created = self.Pickledb.get_or_create(
            pickle_value=pdata, pickle_name='3')
        except peewee.IntegrityError:
            print("Pickle already exists")


        db_pickles = self.Pickledb.select()
        for pickle_db in db_pickles:
            self.balanced_bagging = pickle.loads(pickle_db.pickle_value)

            self.balanced_bagging.fit(X, y)
            print(self.balanced_bagging.predict([[-0.8, -1]]))








if __name__=="__main__":
  Pickle_database()