from io import BytesIO
import io

import sys
print("{}".format(sys.path))
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, A4
from reportlab.lib.units import cm, mm
import PIL

from reportlab.platypus.flowables import Image
from reportlab.lib.utils import ImageReader

def hello(c):
    c.drawString(0, 0, "Hello World")


c = canvas.Canvas("hello.pdf",pagesize=A4,bottomup=0)
c.translate(32 * mm, 19 * mm)

img2 = BytesIO()

cam = cv2.VideoCapture(0)
cv2.namedWindow("test")
ret, frame = cam.read()
cv2.imshow("test", frame)

img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
im_pil = PIL.Image.fromarray(img)
# image_data = io.BytesIO()
# im_pil.save(image_data, format = 'png')

hello(c)
c.drawImage(ImageReader(im_pil),x=10,y=10,width=100*mm,height=100*mm)

c.showPage()
c.save()

cam.release()

cv2.destroyAllWindows()