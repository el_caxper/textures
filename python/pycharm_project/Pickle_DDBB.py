from peewee import *
import datetime
import os
from playhouse.fields import PickleField

# actual_dir = os.path.dirname(os.path.abspath(__file__))
# db = SqliteDatabase(actual_dir+'/my_database.db')
db = SqliteDatabase(None)

class BaseModel(Model):
    class Meta:
        database = db

class Pickle(BaseModel):
    pickle_name = CharField(unique=True)
    pickle_value = PickleField(unique=True)

