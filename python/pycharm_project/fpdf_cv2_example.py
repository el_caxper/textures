from pyfpdf.fpdf.fpdf import FPDF
import sys
print("{}".format(sys.path))
if '/opt/ros/kinetic/lib/python2.7/dist-packages' in sys.path: sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
title = 'Classification Report'

from PIL import Image
import io

A4_HEIGHT = 297
A4_WIDTH = 210

class PDF(FPDF):
    def header(self):
        # Arial bold 15
        self.set_font('Arial', 'B', 15)
        # Calculate width of title and position
        w = self.get_string_width(title) + 6
        self.set_x((210 - w) / 2)
        # Colors of frame, background and text
        self.set_draw_color(0, 0, 255)
        self.set_fill_color(230, 230, 0)
        self.set_text_color(220, 50, 50)
        # Thickness of frame (1 mm)
        self.set_line_width(1)
        # Title
        self.cell(w, 9, title, 1, 1, 'C', 1)
        # Line break
        self.ln(10)

    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Text color in gray
        self.set_text_color(128)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()), 0, 0, 'C')

    def chapter_title(self, num, label):
        # Arial 12
        self.set_font('Arial', '', 12)
        # Background color
        self.set_fill_color(200, 220, 255)
        # Title
        self.cell(0, 6, 'Chapter %d : %s' % (num, label), 0, 1, 'L', 1)
        # Line break
        self.ln(4)

    def chapter_body(self, name):
        # Read text file
        with open(name, 'rb') as fh:
            txt = fh.read().decode('latin-1')
        # Times 12
        self.set_font('Times', '', 12)
        # Output justified text
        self.multi_cell(0, 5, txt)
        # Line break
        self.ln()
        # Mention in italics
        self.set_font('', 'I')
        self.cell(0, 5, '(end of excerpt)')

    def print_chapter(self, num, title, name):
        self.add_page()
        self.chapter_title(num, title)
        self.chapter_body(name)

    def print_image(self, name):
        pass


if __name__ == '__main__':
    pdf = PDF()
    pdf.set_margins(left=32,top=19,right=19)
    pdf.set_title(title)
    pdf.set_author('Jules Verne')
    pdf.print_chapter(1, 'A RUNAWAY REEF', 'Sample.py')
    pdf.print_chapter(2, 'THE PROS AND CONS', 'plot_gabor.py')

    cam = cv2.VideoCapture(0)
    cv2.namedWindow("test")
    ret, frame = cam.read()
    cv2.imshow("test", frame)

    img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    im_pil = Image.fromarray(img)
    image_data = io.BytesIO()
    im_pil.save(image_data, format = 'png')
    pdf.add_page()
    # pdf.image('arbitrary_name_of_' + 'fsa.png', x = 0, y = 0,
    #                 w = im_pil.size[0], h = im_pil.size[1],
    #                 type = '', link = None, file = image_data)

    pdf.image('arbitrary_name_of_' + 'fsa.png',
                    w = 50, h = 50, x=int(A4_WIDTH/2-50/2),
                    type = '', link = None, file = image_data)
    pdf.output('tuto3_image.pdf', 'F')

    #Pasar a python este código de php para centrar la imagen
    #https://gist.github.com/benshimmin/4088493

    # k = cv2.waitKey(0)
    cam.release()

    cv2.destroyAllWindows()