import sys

from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import QFile, QTextStream
from PySide2.QtWidgets import QShortcut, QProgressDialog, QMessageBox
from PySide2.QtGui import QKeySequence

from gui_features_extractor import Ui_MainWindow

from os import listdir
from os.path import isfile, join
import os.path
import numpy as np

import mahotas as mt

from Sample import Sample as Sampledb
from Sample import Image as Imagedb
from Sample import Feature as Featuredb

import peewee

from parameters import DATABASE_NAME, ORIENTATIONS

if '/opt/ros/kinetic/lib/python2.7/dist-packages' in sys.path:
    sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist'
                    '-packages')

import cv2


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.move(0, 0)
        self.setupUi(self)
        self.setWindowIcon(QtGui.QIcon(join(os.path.dirname(os.path.abspath(__file__)), 'icon.ico')))

        self.menubar.setNativeMenuBar(False)

        self.path_to_database = ''
        self.database_name = ''

        self.actionExit.triggered.connect(self.close)
        self.btn_folder.clicked.connect(self.select_folder)
        self.btn_features.clicked.connect(self.get_features)

        self.btn_features.setDisabled(True)

        self.le_distance.setText(str(5))

        self.shortcutOpen = QShortcut(QKeySequence("Ctrl+O"), self)
        self.shortcutOpen.activated.connect(self.select_folder)

        self.shortcutClose = QShortcut(QKeySequence("Ctrl+Q"), self)
        self.shortcutClose.activated.connect(self.close)

        self.coordinates = None
        self.coordinates_patches = None
        self.PATCH_SIZE = None
        self.samples = None
        self.labels = None
        self.images_samples = None
        self.Sample = None
        self.Image = None
        self.Feature = None
        self.features = None
        self.actual_sample = None
        self.features_values_np = None
        self.m_image = None
        self.distance = None
        self.dic_features_values = None
        self.actual_label = None
        self.features_values = None

    def select_folder(self):
        selected_directory = QtWidgets.QFileDialog.getExistingDirectory()

        if selected_directory == '':
            print('Directory not found')

        else:
            self.path_to_database = selected_directory
            print("Path selected:", self.path_to_database)

            num_database_files = [file.split('.')[1] for file in sorted(listdir(selected_directory)) if
                                  isfile(join(selected_directory, file)) and file.endswith(".db")].count('db')
            database_files = [file for file in sorted(listdir(selected_directory)) if
                              isfile(join(selected_directory, file)) and file.endswith(".db")]

            print("Number of databases in path:", num_database_files)
            print("Databases names in path:", database_files)

            if DATABASE_NAME in database_files:
                print("Exist normal database:", DATABASE_NAME)
                self.database_name = DATABASE_NAME
            elif num_database_files > 0:
                print("Database selected:", database_files[0])
                self.database_name = database_files[0]

            if self.database_name != '':
                self.btn_features.setEnabled(True)

    # function to extract haralick textures from an image
    def extract_features(self, image):
        # calculate haralick texture features for 4 types of adjacency
        textures = mt.features.haralick(image, compute_14th_feature=True, distance=self.distance)

        # take the mean of it and return it
        textures_selected = []
        for i in ORIENTATIONS:
            if i == 1:
                assert isinstance(textures, object)
                textures_selected.append(textures[i, :])

        textures_selected = np.array(textures_selected)

        ht_mean = textures_selected.mean(axis=0)
        return ht_mean

    def get_features(self):

        if self.le_distance.text() == '':
            print("Empty Distance")
            self.distance = 5
        else:
            self.distance = int(self.le_distance.text())

        try:
            from Sample import db
            db.init(join(self.path_to_database, self.database_name))
            self.Sample = Sampledb()
            self.Image = Imagedb()
            self.Feature = Featuredb()
            self.Sample.create_table()
            self.Image.create_table()
            self.Feature.create_table()
        except peewee.OperationalError:
            print("tables already exist!")

        # delete all features in database table
        db_features = self.Feature.select()

        if len(db_features) > 0:
            msg_box = QMessageBox(self)
            msg_box.setWindowTitle("Features Extractor")
            msg_box.setText("There already exist features in database")
            msg_box.setInformativeText("Do you want to recalculate the features?")
            msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            msg_box.setDefaultButton(QMessageBox.No)
            msg_box.setEscapeButton(QMessageBox.No)
            ret = msg_box.exec_()

            if ret == QMessageBox.No:
                return
            else:
                pass

        for feature_db in db_features:
            feature_db.delete_instance()

        db_images = self.Image.select()
        print("number of images:", len(db_images))

        self.coordinates = []
        self.coordinates_patches = []
        self.PATCH_SIZE = []
        self.samples = []
        self.labels = []
        self.images_samples = []

        for image in db_images:

            samples_in_image = image.samples
            print("number of samples:", len(samples_in_image))
            for sample in samples_in_image:
                print("sample label: {0}".format(sample.label))
                print("sample id: {0}".format(sample.get_id()))
                print("image name: {0}".format(image.image_name))

                self.features = sample.features
                self.actual_sample = sample
                self.samples.append(sample)
                self.labels.append(sample.label)

                image_cv2 = cv2.imread(join(self.path_to_database,
                                            image.image_name))
                self.m_image = cv2.cvtColor(image_cv2, cv2.COLOR_BGR2GRAY)
                self.images_samples.append(self.m_image)

                self.PATCH_SIZE.append(sample.size)
                self.coordinates.append((sample.y, sample.x))

        for index, (loc, patch) in enumerate(zip(self.coordinates, self.PATCH_SIZE)):
            self.coordinates_patches.append(self.images_samples[index][loc[0] - int(patch / 2):loc[0] + int(patch / 2),
                                            loc[1] - int(patch / 2):loc[1] + int(patch / 2)])

        progress = QProgressDialog("Extracting features ...", "", 0, len(self.coordinates_patches), self)
        progress.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        progress.setCancelButton(None)
        progress.setWindowTitle("Features Extractor")
        progress.setWindowModality(QtCore.Qt.WindowModal)
        progress.setStyleSheet("background-color:rgba(220, 220, 220, 255);")

        progress.show()
        progress_val = 0

        windows_name = "samples"
        if self.cb_debug.isChecked():
            cv2.namedWindow(windows_name)
            cv2.moveWindow(windows_name, 550, 20)
            self.dic_features_values = {}
            self.features_values = []
            self.actual_label = None

        for i, patch in enumerate(self.coordinates_patches):
            if self.cb_debug.isChecked():
                try:
                    cv2.imshow(windows_name, patch)
                    cv2.waitKey(1)
                except Exception as e:
                    print("Error:", e)
            # cv2.destroyAllWindows()

            try:
                progress.setValue(progress_val)
                progress_val += 1
                features = self.extract_features(patch)

                self.Feature.get_or_create(sample=self.samples[i],
                                           label=self.labels[i],
                                           ASM=features[0],
                                           Contrast=features[1],
                                           Correlation=features[2],
                                           Sum_of_Squares_Variance=features[3],
                                           Inverse_Difference_Moment=features[4],
                                           Sum_Average=features[5],
                                           Sum_Variance=features[6],
                                           Sum_Entropy=features[7],
                                           Entropy=features[8],
                                           Difference_Variance=features[9],
                                           Difference_Entropy=features[10],
                                           Information_Measure_of_Correlation_1=features[11],
                                           Information_Measure_of_Correlation_2=features[12],
                                           Maximal_Correlation_Coefficient=features[13])
                if self.cb_debug.isChecked():
                    if not self.labels[i] in self.dic_features_values:
                        if self.actual_label:
                            self.dic_features_values[self.actual_label] = self.features_values
                        self.actual_label = self.labels[i]
                        self.features_values = []

                    self.features_values.append(
                        [features[0], features[1], features[2], features[3], features[4], features[5], features[6],
                         features[7], features[8], features[9], features[10], features[11], features[12], features[13]])
            except Exception as e:
                print("Error:", e)

        if self.cb_debug.isChecked():
            cv2.destroyAllWindows()
            self.dic_features_values[self.labels[len(self.coordinates_patches) - 1]] = self.features_values
            for key, value in self.dic_features_values.items():  # Iterates through the pairs
                self.features_values_np = np.array(value)
                np.set_printoptions(suppress=True, precision=5)
                assert isinstance(key, object)
                print("key: {}, mean: {}".format(key, self.features_values_np.mean(axis=0)))
        print("FINISH FEATURES EXTRACTION")
        progress.close()


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    my_path = os.path.abspath(os.path.dirname(__file__))
    filestyle = os.path.join(my_path, 'Minimalist_ok.qss')
    f = QFile(filestyle)
    f.open(QFile.ReadOnly | QFile.Text)
    ts = QTextStream(f)
    stylesheet = ts.readAll()
    app.setStyleSheet(stylesheet)
    frame = MainWindow()
    frame.show()
    sys.exit(app.exec_())
