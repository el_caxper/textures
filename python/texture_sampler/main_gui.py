import os.path
from os import listdir
from os.path import isfile, join

import numpy as np
import peewee
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import QFile, QTextStream
from PySide2.QtGui import QPixmap, QImage, QPainter, QKeySequence
from PySide2.QtWidgets import QShortcut

from Sample import Image as Imagedb
from Sample import Sample as Sampledb
from class_sampler_gui import Ui_TextureSampler
from parameters import EXTENSIONS, EXTENSIONS_NO_POINT, DATABASE_NAME
from utils import path_leaf


class MainWindow(QtWidgets.QMainWindow, Ui_TextureSampler):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QtGui.QIcon(join(os.path.dirname(os.path.abspath(__file__)), 'icon.ico')))

        # self.tb_sample.horizontalHeader().setStretchLastSection(True)
        header = self.tb_sample.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(3, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(4, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(5, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(6, QtWidgets.QHeaderView.Stretch)

        self.tb_sample.verticalHeader().setVisible(False)
        self.le_size.setText(str(20))

        self.menubar.setNativeMenuBar(False)
        self.actionOpen_Directory.triggered.connect(self.select_directory)
        self.actionQuit.triggered.connect(self.close)

        self.click = 0
        self.actual_number_image = 0
        self.cb_positive.setChecked(True)
        # self.showMaximized()
        self.actual_dir = os.path.dirname(os.path.abspath(__file__))

        self.lb_image.mousePressEvent = self.get_pos

        index = QtCore.QPersistentModelIndex(self.tb_sample.model().index(-1, 6))
        self.btn_undo.clicked.connect(
            lambda *args, index_=index: self.on_button(index_.row(), index_.column()))

        self.btn_next_image.clicked.connect(self.on_next_image)

        self.btn_prev_image.clicked.connect(self.on_prev_image)

        self.samples_list = [[]]

        self.shortcutOpen = QShortcut(QKeySequence("Ctrl+O"), self)
        self.shortcutOpen.activated.connect(self.select_directory)
        self.shortcutClose = QShortcut(QKeySequence("Ctrl+Q"), self)
        self.shortcutClose.activated.connect(self.close)
        self.shortcutUndo = QShortcut(QKeySequence("Ctrl+Z"), self)
        self.shortcutUndo.activated.connect(
            lambda *args, index_=index: self.on_button(index_.row(), index_.column()))

        self.shortcutNext = QShortcut(QKeySequence("Ctrl+<"), self)
        self.shortcutNext.activated.connect(self.on_next_image)

        self.shortcutPrev = QShortcut(QKeySequence("Ctrl+Shift+<"), self)
        self.shortcutPrev.activated.connect(self.on_prev_image)

        self.pixel_map = None
        self.list_images = None
        self.num_labels = None
        self.target = None
        self.image = None
        self.my_samples_db = None
        self.real_image_size = None
        self.imagedb = None
        self.image_db_created = None
        self.actual_image = None
        self.selected_directory = None
        self.data = None
        self.Sample = None
        self.Image = None

    def on_next_image(self):
        if self.actual_number_image + 1 < len(self.list_images):
            self.actual_number_image += 1
            self.samples_list.append([])
            self.set_image(self.list_images[self.actual_number_image])
            self.plot_samples()
            self.update_table()

    def on_prev_image(self):
        if self.actual_number_image - 1 > -1:
            self.actual_number_image -= 1
            self.set_image(self.list_images[self.actual_number_image])
            self.plot_samples()
            self.update_table()

    def get_pos(self, event):
        if self.le_label.text() == "":
            self.results()
        else:
            x = event.pos().x()
            y = event.pos().y()
            print("pixel: x:", x, " y: ", y)
            print("ancho real: {}, largo real: {}".format(self.real_image_size[0], self.real_image_size[1]))

            x = int(self.real_image_size[0] * x / self.lb_image.width())
            y = int(self.real_image_size[1] * y / self.lb_image.height())

            print("pos x real: {}, pos y real: {}".format(x, y))

            self.samples_list[self.actual_number_image].append([self.le_label.text(), x, y,
                                                                self.le_size.text(), self.cb_positive.isChecked()])
            self.plot_samples()

            self.Sample.get_or_create(image=self.imagedb,
                                      label=self.le_label.text(), x=x, y=y,
                                      size=self.le_size.text(),
                                      positive=int(self.cb_positive.isChecked()))

    def plot_samples(self):

        self.pixel_map = self.actual_image
        h = self.lb_image.height()
        self.pixel_map = self.pixel_map.scaled(1000000, h, QtCore.Qt.KeepAspectRatio)

        self.lb_image.setPixmap(self.pixel_map)
        self.lb_image.update()

        for sample in self.samples_list[self.actual_number_image]:

            h = self.lb_image.height()
            self.pixel_map = self.pixel_map.scaled(1000000, h, QtCore.Qt.KeepAspectRatio)
            self.lb_image.setPixmap(self.pixel_map)
            self.lb_image.update()

            q_painter = QPainter(self.pixel_map)

            q_painter.setBrush(QtCore.Qt.NoBrush)
            if sample[4] == 1:
                q_painter.setPen(QtGui.QPen(QtCore.Qt.green, 4))
            else:
                q_painter.setPen(QtGui.QPen(QtCore.Qt.red, 4))
            rx = int(float(sample[1]) * self.lb_image.width() / self.real_image_size[0])
            ry = int(float(sample[2]) * self.lb_image.height() / self.real_image_size[1])

            resize = int(float(sample[3]) * self.lb_image.height() / self.real_image_size[1])
            print("Sample[1]: {}".format(sample[1]))
            print("Sample[2]: {}".format(sample[2]))
            print("Sample[3]: {}".format(sample[3]))
            print("self.lb_image.width: {}".format(self.lb_image.width()))
            print("self.real_image_size[0]: {}".format(self.real_image_size[0]))

            print("Plot sample in x: {}, y: {}, size: ".format(rx - int(resize) / 2, ry - int(resize) / 2, int(resize)))

            q_painter.drawRect(rx - int(resize) / 2, ry - int(resize) / 2,
                               int(resize),
                               int(resize))

            q_painter.end()

            self.lb_image.setPixmap(self.pixel_map)

        self.update_table()

    def results(self):
        QtWidgets.QMessageBox.about(self, "Empty label",
                                    "Label can not be empty")

    def select_directory(self):
        self.selected_directory = QtWidgets.QFileDialog.getExistingDirectory()

        if self.selected_directory == '':
            print('Directory not found')
        else:

            num_dat_files = [file.split('.')[1] for file in sorted(listdir(self.selected_directory)) if
                             isfile(join(self.selected_directory, file)) and file.endswith(EXTENSIONS)]

            assert isinstance(EXTENSIONS_NO_POINT, object)
            num_dat_files = sum(map(num_dat_files.count, EXTENSIONS_NO_POINT))

            if num_dat_files > 0:

                only_files = [file for file in sorted(listdir(self.selected_directory)) if
                              isfile(join(self.selected_directory, file)) and file.endswith(EXTENSIONS)]
                self.target = np.array(only_files)

                self.num_labels = len(set(only_files))

                print('selected_directory:', self.selected_directory)
                print('num_dat_files:', num_dat_files)
                print('num_labels:', self.num_labels)

                self.list_images = [join(self.selected_directory, filename)
                                    for filename in sorted(listdir(self.selected_directory)) if
                                    (isfile(join(self.selected_directory, filename)) and filename.endswith(EXTENSIONS))]
                self.data = np.array(self.list_images)
                print('Size', self.data.size)

                try:
                    from Sample import db
                    db.init(join(self.selected_directory, DATABASE_NAME))
                    self.Sample = Sampledb()
                    self.Image = Imagedb()
                    self.Sample.create_table()
                    self.Image.create_table()
                except peewee.OperationalError:
                    print("tables already exist!")

                self.set_image(self.list_images[0])

    def set_image(self, name):
        self.image = QImage(3, 3, QImage.Format_RGB32)
        self.image.load(name)
        self.real_image_size = (self.image.size().width(), self.image.size().height())
        print("ancho real: {}, largo real: {}".format(self.real_image_size[0], self.real_image_size[1]))
        self.pixel_map = QPixmap(self.image)
        h = self.lb_image.height()
        self.pixel_map = self.pixel_map.scaled(1000000, h, QtCore.Qt.KeepAspectRatio)
        self.actual_image = self.pixel_map
        self.lb_image.setPixmap(self.pixel_map)
        self.lb_image.setGeometry(QtCore.QRect(0, 0, self.pixel_map.width(), self.pixel_map.height()))
        self.imagedb, self.image_db_created = self.Image.get_or_create(
            image_name=path_leaf(self.list_images[self.actual_number_image]))

        if not self.image_db_created:
            self.my_samples_db = self.imagedb.samples

            if len(self.my_samples_db) > 0:

                if not self.samples_list[self.actual_number_image]:
                    for i in self.my_samples_db:
                        self.samples_list[self.actual_number_image].append([i.label, i.x, i.y,
                                                                            i.size, i.positive])
                        print(i.label)
                self.plot_samples()
                self.update_table()

    def update_table(self):
        for i in reversed(range(self.tb_sample.rowCount())):
            self.tb_sample.removeRow(i)
        for num, sample in enumerate(self.samples_list[self.actual_number_image]):
            label = sample[0]
            x = sample[1]
            y = sample[2]
            size = sample[3]
            positive = sample[4]
            row_position = self.tb_sample.rowCount()
            self.tb_sample.insertRow(row_position)
            item = QtWidgets.QTableWidgetItem(str(row_position))
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            item.setFlags(item.flags() ^ QtCore.Qt.ItemIsEditable)
            self.tb_sample.setItem(row_position, 0, item)
            item = QtWidgets.QTableWidgetItem(str(label))
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            item.setFlags(item.flags() ^ QtCore.Qt.ItemIsEditable)
            self.tb_sample.setItem(row_position, 1, item)
            item = QtWidgets.QTableWidgetItem(str(x))
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            item.setFlags(item.flags() ^ QtCore.Qt.ItemIsEditable)
            self.tb_sample.setItem(row_position, 2, item)
            item = QtWidgets.QTableWidgetItem(str(y))
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            item.setFlags(item.flags() ^ QtCore.Qt.ItemIsEditable)
            self.tb_sample.setItem(row_position, 3, item)
            item = QtWidgets.QTableWidgetItem(str(size))
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            item.setFlags(item.flags() ^ QtCore.Qt.ItemIsEditable)
            self.tb_sample.setItem(row_position, 4, item)

            q_widget = QtWidgets.QWidget()
            checkbox = QtWidgets.QCheckBox()
            checkbox.setChecked(positive)
            index = QtCore.QPersistentModelIndex(self.tb_sample.model().index(row_position, 5))
            checkbox.clicked.connect(
                lambda *args, index_=index: self.on_check(index_.row(), index_.column()))
            qhboxlayout = QtWidgets.QHBoxLayout(q_widget)
            qhboxlayout.addWidget(checkbox)
            qhboxlayout.setAlignment(QtCore.Qt.AlignCenter)
            qhboxlayout.setContentsMargins(0, 0, 0, 0)
            self.tb_sample.setCellWidget(row_position, 5, q_widget)

            qwidget2 = QtWidgets.QWidget()
            btn = QtWidgets.QPushButton()
            btn.setText("Remove")
            index = QtCore.QPersistentModelIndex(self.tb_sample.model().index(row_position, 6))
            btn.clicked.connect(
                lambda *args, index_=index: self.on_button(index_.row(), index_.column()))
            qhboxlayout2 = QtWidgets.QHBoxLayout(qwidget2)
            qhboxlayout2.addWidget(btn)
            qhboxlayout2.setAlignment(QtCore.Qt.AlignCenter)
            qhboxlayout2.setContentsMargins(0, 0, 0, 0)
            self.tb_sample.setCellWidget(row_position, 6, qwidget2)

    def on_button(self, n, m):
        if not self.samples_list[self.actual_number_image]:
            return
        else:

            self.Sample.get(label=self.samples_list[self.actual_number_image][n][0],
                            x=self.samples_list[self.actual_number_image][n][1],
                            y=self.samples_list[self.actual_number_image][n][2],
                            size=self.samples_list[self.actual_number_image][n][3],
                            positive=self.samples_list[self.actual_number_image][n][4]) \
                .delete_instance()

            del self.samples_list[self.actual_number_image][n]
            print('Button {0} {1} clicked'.format(n, m))

            self.plot_samples()
            self.update_table()

    def on_check(self, n, m):
        print('Check {0}  clicked'.format(self.samples_list[self.actual_number_image][n][4]))
        print(m)

        self.samples_list[self.actual_number_image][n][4] = not (self.samples_list[self.actual_number_image][n][4])


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    my_path = os.path.abspath(os.path.dirname(__file__))
    file_style = os.path.join(my_path, 'Minimalist_ok.qss')
    f = QFile(file_style)
    f.open(QFile.ReadOnly | QFile.Text)
    ts = QTextStream(f)
    stylesheet = ts.readAll()
    app.setStyleSheet(stylesheet)
    frame = MainWindow()
    frame.show()
    sys.exit(app.exec_())
