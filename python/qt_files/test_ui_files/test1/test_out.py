#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Qt requirements"""

import os

from PySide2 import QtCore, QtGui, QtWidgets

from output import Ui_MainWindow
from PySide2.QtCore import QUrl

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.pushButton.clicked.connect(self.resultados)
        self.click=0

        self.webView.load(QUrl("https://www.google.com/"))
        self.webView.show()


    def resultados(self):
        QtWidgets.QMessageBox.about(self, "Resultados",
                                "{} \n Esperado: {}, Predicho: {} ".format("Resultado",
                                                   "Estos","Aquello"),
                                )

        self.click= not self.click

        if(self.click==True):
            self.label.setText("Adios")
        else:
            self.label.setText("Hola")

        self.webView.load(QUrl("https://www.google.com/"))
        self.webView.show()

        
        


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    frame = MainWindow()
    frame.show()
    sys.exit(app.exec_())

