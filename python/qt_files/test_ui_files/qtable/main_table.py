#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Qt requirements"""

import os

from PySide2 import QtCore, QtGui, QtWidgets
from test_ui_files.qtable.class_table import Ui_MainWindow
from PySide2.QtCore import QUrl
from PySide2.QtGui import QPixmap, QImage, QPainter

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.click=0

        self.tableWidget.setRowCount(10)
        self.tableWidget.setColumnCount(5)

        qwidget = QtWidgets.QWidget()
        checkbox = QtWidgets.QCheckBox()
        checkbox.setCheckState(QtCore.Qt.Unchecked)
        qhboxlayout = QtWidgets.QHBoxLayout(qwidget)
        qhboxlayout.addWidget(checkbox)
        qhboxlayout.setAlignment(QtCore.Qt.AlignCenter)
        qhboxlayout.setContentsMargins(0, 0, 0, 0)
        self.tableWidget.setCellWidget(1, 1, qwidget)

        checkbox.clicked.connect(self.doCheck)

    def doCheck(self):
        print("Click checkbox")

        #self.tableWidget.setItem(1, 1, QtWidgets.QTableWidgetItem(str(1)))



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    frame = MainWindow()
    frame.show()
    sys.exit(app.exec_())

