#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Qt requirements"""

import os

from PySide2 import QtCore, QtGui, QtWidgets
from test_ui_files.qlabel_image.class_image import Ui_MainWindow
from PySide2.QtCore import QUrl
from PySide2.QtGui import QPixmap, QImage, QPainter

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.click=0

        self.image = QImage(3, 3, QImage.Format_RGB32)
        self.image.load("./index.png")
        size_img = self.image.size()
        self.pixmap = QPixmap(self.image)
        #pixmap = pixmap.scaled(h, w, Qt.KeepAspectRatio)
        self.qlabel_image.resize(size_img)
        self.qlabel_image.setPixmap(self.pixmap)
        self.qlabel_image.setScaledContents(True)

        self.qlabel_image.mousePressEvent = self.getPos

    def getPos(self, event):
        x = event.pos().x()
        y = event.pos().y()
        print("pixel: x:", x, " y: ", y)

        qPainter = QPainter (self.pixmap)

        qPainter.setBrush(QtCore.Qt.NoBrush)
        qPainter.setPen(QtCore.Qt.red)
        qPainter.drawRect(x-10, y-10, 20, 20)

        #qPainter.eraseRect(x-10, y-10, 20, 20)
        qPainter.end()
        #self.pixmap = QPixmap(self.image)

        self.qlabel_image.setPixmap(self.pixmap)

    def resultados(self):
        QtWidgets.QMessageBox.about(self, "Resultados",
                                "{} \n Esperado: {}, Predicho: {} ".format("Resultado",
                                                   "Estos","Aquello"),
                                )

        self.click= not self.click

        if(self.click==True):
            self.label.setText("Adios")
        else:
            self.label.setText("Hola")

        self.webView.load(QUrl("https://www.google.com/"))
        self.webView.show()

        
        


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    frame = MainWindow()
    frame.show()
    sys.exit(app.exec_())

