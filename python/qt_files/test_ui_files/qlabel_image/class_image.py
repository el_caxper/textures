# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'image.ui',
# licensing of 'image.ui' applies.
#
# Created: Wed Jun 27 22:54:23 2018
#      by: pyside2-uic  running on PySide2 5.9.0a1.dev1528453888
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(567, 454)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.qlabel_image = QtWidgets.QLabel(self.centralwidget)
        self.qlabel_image.setGeometry(QtCore.QRect(70, 70, 171, 141))
        self.qlabel_image.setObjectName("qlabel_image")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 567, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "MainWindow", None, -1))
        self.qlabel_image.setText(QtWidgets.QApplication.translate("MainWindow", "TextLabel", None, -1))

