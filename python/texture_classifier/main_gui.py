import math
import time

from sklearn.decomposition import TruncatedSVD
from sklearn import svm, metrics
from sklearn.externals import joblib
from sklearn import __version__ as skver
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from imblearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.multiclass import OneVsRestClassifier

import mahotas as mt

from itertools import chain

import os
import sys

from main_window import Ui_MainWindow

from PySide2 import QtWidgets
from PySide2 import QtGui
from PySide2.QtCore import QFile, QTextStream
from PySide2.QtWidgets import QInputDialog, QLineEdit, QShortcut
from PySide2.QtGui import QKeySequence

from os import listdir
from os.path import isfile, join
import os.path
import numpy as np

import peewee
from Sample import Sample as Sampledb
from Sample import Image as Imagedb
from Sample import Feature as Featuredb
from Sample import Pickle as Pickledb

import pickle

from params_dialog import DialogWithCheckBox

from pdf_sample import PDF

from parameters import RANDOM_STATE, A4_WIDTH, K_SELECT, MANUAL_SELECT, EXTENSIONS, EXTENSIONS_NO_POINT, ORIENTATIONS

from PIL import Image
import io

if '/opt/ros/kinetic/lib/python2.7/dist-packages' in sys.path: sys.path.remove(
    '/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2

ADD_NO_TO_SAMPLES = 0
DELETE_REPEATED_LABELS = 1


def extract_features(image, distance):
    # calculate haralick texture features for 4 types of adjacency
    textures = mt.features.haralick(image, compute_14th_feature=True, distance=distance)

    # take the mean of it and return it

    textures_selected = []

    for i in ORIENTATIONS:
        if i == 1:
            textures_selected.append(textures[i, :])

    textures_selected = np.array(textures_selected)

    ht_mean = textures_selected.mean(axis=0)
    return ht_mean


def image_resize(image, width=None, height=None, inter=cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    (h, w) = image.shape[:2]
    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image
    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)
    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))
    # resize the image
    resized = cv2.resize(image, dim, interpolation=inter)
    # return the resized image
    return resized


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.move(0, 0)
        self.setupUi(self)

        screen_resolution = QtWidgets.QDesktopWidget().screenGeometry()
        self.screen_width, self.screen_height = screen_resolution.width(), screen_resolution.height()
        print("Ancho: {}, Alto: {}".format(self.screen_width, self.screen_height))

        self.setWindowIcon(QtGui.QIcon(join(os.path.dirname(os.path.abspath(__file__)), 'icon.ico')))

        print('sklearn version: {}'.format(skver))

        self.menubar.setNativeMenuBar(False)

        self.btn_clasificar.setDisabled(True)
        self.btn_clasificar_folder.setDisabled(True)
        self.btn_detalles.setDisabled(True)
        self.btn_detalles.clicked.connect(self.resultados)
        self.btn_entrenar.setDisabled(True)
        self.btn_entrenar.clicked.connect(self.entrenar)
        self.btn_clasificar.clicked.connect(self.on_input_file_button_clicked)
        self.btn_clasificar_folder.clicked.connect(self.selectDirectory_folder)
        self.btn_parameters.clicked.connect(self.select_parameters)

        self.actionAbrir.triggered.connect(self.select_directory)

        self.actionGuardar_SVM.triggered.connect(self.save_svm)
        self.actionCargar_SVM.triggered.connect(self.load_svm)

        self.shortcutOpen = QShortcut(QKeySequence("Ctrl+O"), self)
        self.shortcutOpen.activated.connect(self.select_directory)
        self.shortcutSave = QShortcut(QKeySequence("Ctrl+S"), self)
        self.shortcutSave.activated.connect(self.save_svm)
        self.shortcutLoad = QShortcut(QKeySequence("Ctrl+L"), self)
        self.shortcutLoad.activated.connect(self.load_svm)

        self.selector = None

        self.lista_kernels = [
            'Linear',
            'Polynomial',
            'RBF',
            'Sigmoid'
        ]

        self._lista_kernels = [
            'linear',
            'poly',
            'rbf',
            'sigmoid'
        ]

        self.cb_kernel.addItems(self.lista_kernels)
        self.le_gamma.setText('10')
        self.le_C.setText('100000000')

        self.cb_kernel.currentIndexChanged.connect(self.cb_kernel_change)
        self.cb_degree.currentIndexChanged.connect(self.cb_degree_change)
        self.cb_degree.setDisabled(True)
        self.le_gamma.setDisabled(True)
        self.lb_gamma.setText('Gamma')
        self.kernel_selected = 0

        self.halarick_distance = 5
        self.sample_size = 150
        self.debug_mode = True
        self.save_data_pdf = True
        self.features_mode = MANUAL_SELECT
        self.features_number = 14

        self.Sample = None
        self.Image = None
        self.Feature = None
        self.Pickle_db = None
        self.data = None
        self.num_labels = None
        self.target = None
        self.labels = None
        self.data2 = None
        self.target2 = None
        self.multi_label_biniarizer = None
        self.degree_selected = None
        self.m_image = None
        self.m_image_size = None
        self.m_image_draw = None
        self.selected_directory = None
        self.balanced_bagging = None
        self.coordinates_patches = None
        self.coordinates = None
        self.my_data = None
        self.list_save_pdf = None
        self.m_image_draw2 = None
        self.m_image_draw3 = None
        self.pickle_db_created = None
        self.predicted_value = None
        self.pickle_db = None

    def select_parameters(self):
        dialog = DialogWithCheckBox(self)
        dialog.setModal(True)
        result = dialog.exec_()

        if result[0] == 1:
            if result[3] != '':
                self.halarick_distance = int(result[3])

            if result[4] != '':
                self.sample_size = int(result[4])

            self.debug_mode = result[1]
            self.save_data_pdf = result[2]

            if result[5] == 0:
                self.features_mode = MANUAL_SELECT
                self.features_number = 14
                if result[6] == 'all':
                    pass
            else:
                self.features_mode = K_SELECT
                self.features_number = int(result[6])
                print("Features selected: {}".format(self.features_number))

            print("Accepted")
        else:
            print("Closed")

    def init_database(self, onlyfiles):
        try:
            from Sample import db
            db.init(join(self.selected_directory, onlyfiles[0]))
            self.Sample = Sampledb()
            self.Image = Imagedb()
            self.Feature = Featuredb()
            self.Pickle_db = Pickledb()
            self.Sample.create_table()
            self.Image.create_table()
            self.Feature.create_table()
            self.Pickle_db.create_table()
        except peewee.OperationalError:
            print("tables already exist!")

    def select_directory(self):
        self.selected_directory = QtWidgets.QFileDialog.getExistingDirectory()

        if self.selected_directory == '':
            print('directorio incorrecto incorrecto')

        else:

            num_dat_files = [file.split('.')[1] for file in sorted(listdir(self.selected_directory)) if
                             isfile(join(self.selected_directory, file)) and file.endswith(".db")].count('db')

            if num_dat_files > 0:

                onlyfiles = [file.split('1')[0] for file in sorted(listdir(self.selected_directory)) if
                             isfile(join(self.selected_directory, file)) and file.endswith(".db")]

                self.init_database(onlyfiles)

                db_features = self.Feature.select()
                targets = []
                datas = []
                datas_negative = []
                negative_samples = []
                for feature_db in db_features:
                    if feature_db.sample.positive:
                        targets.append(feature_db.label)

                        datas.append([feature_db.ASM,
                                      feature_db.Contrast,
                                      feature_db.Correlation,
                                      feature_db.Sum_of_Squares_Variance,
                                      feature_db.Inverse_Difference_Moment,
                                      feature_db.Sum_Average,
                                      feature_db.Sum_Variance,
                                      feature_db.Sum_Entropy,
                                      feature_db.Entropy,
                                      feature_db.Difference_Variance,
                                      feature_db.Difference_Entropy,
                                      feature_db.Information_Measure_of_Correlation_1,
                                      feature_db.Information_Measure_of_Correlation_1,
                                      feature_db.Maximal_Correlation_Coefficient])
                        print("Label: {0}".format(feature_db.label))

                    else:
                        negative_samples.append(feature_db.label)

                        datas_negative.append([feature_db.ASM,
                                               feature_db.Contrast,
                                               feature_db.Correlation,
                                               feature_db.Sum_of_Squares_Variance,
                                               feature_db.Inverse_Difference_Moment,
                                               feature_db.Sum_Average,
                                               feature_db.Sum_Variance,
                                               feature_db.Sum_Entropy,
                                               feature_db.Entropy,
                                               feature_db.Difference_Variance,
                                               feature_db.Difference_Entropy,
                                               feature_db.Information_Measure_of_Correlation_1,
                                               feature_db.Information_Measure_of_Correlation_1,
                                               feature_db.Maximal_Correlation_Coefficient])
                        print("Label neg: {0}".format(feature_db.label))

                self.target = np.array(targets)
                self.data = np.array(datas)

                labels_array = np.unique(self.target)

                new_targets = []
                if ADD_NO_TO_SAMPLES:
                    for i, label in (enumerate(targets)):
                        new_labels_to_add = [x for x in labels_array if x != targets[i]]
                        new_labels_to_add_no = [s + '_no' for s in new_labels_to_add]
                        new_labels_to_add_no.append(label)
                        new_targets.append(new_labels_to_add_no)
                else:
                    for i, label in (enumerate(targets)):
                        new_targets.append([targets[i]])

                if len(negative_samples) > 0:
                    for i, label in (enumerate(negative_samples)):
                        new_targets.append([label + '_no'])
                    print(new_targets)
                    self.data = np.concatenate((self.data, np.array(datas_negative)), axis=0)

                self.num_labels = len(set(self.target))

                if len(new_targets) > 0:
                    self.target = np.array(new_targets)

                self.labels = np.unique(self.target)
                flat_list = [item for sublist in self.labels for item in sublist]
                self.labels = set(flat_list)

                print(self.data)
                print(self.target)

                print('selected_directory:', self.selected_directory)
                print('num_dat_files:', num_dat_files)
                print('num_labels:', self.num_labels)

                self.cb_degree.clear()
                if self.num_labels + 4 < 10:
                    for i in range(self.num_labels, 15):
                        self.cb_degree.addItem(str(i))
                else:
                    for i in range(5, 10):
                        self.cb_degree.addItem(str(i))
                print('Size', self.data.shape)

                self.btn_entrenar.setDisabled(False)

    def selectDirectory_folder(self):
        self.selected_directory_test = QtWidgets.QFileDialog.getExistingDirectory()

        if self.selected_directory_test == '':
            print('directorio incorrecto incorrecto')

        else:

            num_dat_files = [file.split('.')[1] for file in sorted(listdir(self.selected_directory_test)) if
                             isfile(join(self.selected_directory_test, file)) and file.endswith(EXTENSIONS)]

            num_dat_files = sum(map(num_dat_files.count, EXTENSIONS_NO_POINT))

            if num_dat_files > 0:
                only_files = [file for file in sorted(listdir(self.selected_directory_test)) if
                              isfile(join(self.selected_directory_test, file)) and file.endswith(EXTENSIONS)]
                print(only_files)

                for file in only_files:
                    self.classify_image(join(self.selected_directory_test, file))

    def about(self):
        QtWidgets.QMessageBox.about(self, "About",
                                    "Application Created by Gustavo Plaza Roma "
                                    "for the Trabajo fin de máster. "
                                    "Source code available in "
                                    "<a href='https://bitbucket.org/el_caxper/textures/src'>Repository</a>")

    def resultados(self):
        QtWidgets.QMessageBox.about(self, "Resultados",
                                    "{} \n Esperado: {}, Predicho: {} ".format(
                                        metrics.classification_report(self.expected, self.predicted_value),
                                        self.expected[0], self.predicted_value[0]),
                                    )

    def cb_kernel_change(self, string):
        self.kernel_selected = string
        if string == 0:
            self.cb_degree.setDisabled(True)
            self.le_gamma.setDisabled(True)
            self.lb_gamma.setText('Gamma')
            self.le_gamma.setText(str(0.0001))
        elif string == 1:
            self.cb_degree.setDisabled(False)
            self.lb_gamma.setText('coef0')
            self.le_gamma.setDisabled(False)
            self.le_gamma.setText(str(0.0))
        elif string == 2:
            self.cb_degree.setDisabled(True)
            self.le_gamma.setDisabled(False)
            self.lb_gamma.setText('Gamma')
            self.le_gamma.setText(str(10))
        elif string == 3:
            self.cb_degree.setDisabled(True)
            self.le_gamma.setDisabled(False)
            self.lb_gamma.setText('coef0')
            self.le_gamma.setText(str(0.0))

    def entrenar(self):

        _kernel = self._lista_kernels[self.cb_kernel.currentIndex()]
        _degree = self.degree_selected  # + self.num_labels
        _C_param = float(self.le_C.text())

        print('kernel: ', _kernel)
        print('degree: ', _degree)
        if self.cb_kernel.currentIndex() == 1 or self.cb_kernel.currentIndex() == 3:
            _coef0 = float(self.le_gamma.text())
            print('C param: ', _C_param)
            self.balanced_bagging = OneVsRestClassifier(
                svm.SVC(probability=True, C=_C_param, coef0=_coef0, degree=_degree,
                        kernel=_kernel, cache_size=7000, random_state=RANDOM_STATE), n_jobs=-1)

        else:
            _gamma = float(self.le_gamma.text())

            if self.num_labels > 1:

                if self.features_mode == MANUAL_SELECT:
                    self.balanced_bagging = Pipeline([
                        ('tfidf', TfidfTransformer()),
                        ('clf', OneVsRestClassifier(
                            svm.SVC(probability=True, C=_C_param, gamma=_gamma, kernel=_kernel,
                                    cache_size=70000, random_state=RANDOM_STATE), n_jobs=-1))])
                else:
                    self.balanced_bagging = Pipeline([
                        ('tfidf', TfidfTransformer()),
                        ('pca', TruncatedSVD(n_components=self.features_number)),  # SELECT FEATURES
                        ('clf', OneVsRestClassifier(
                            svm.SVC(probability=True, C=_C_param, gamma=_gamma, kernel=_kernel,
                                    cache_size=70000, random_state=RANDOM_STATE), n_jobs=-1))])

            else:
                self.balanced_bagging = svm.OneClassSVM(nu=_C_param, gamma=_gamma, kernel=_kernel,
                                                        cache_size=70000, random_state=RANDOM_STATE)

        print("Empieza el entrenamientoo")

        print(self.data.shape)

        self.data2 = self.data

        print(self.data2.shape)

        self.data2, self.target2 = shuffle(self.data2, self.target, random_state=RANDOM_STATE)

        self.multi_label_biniarizer = MultiLabelBinarizer()
        y_enc = self.multi_label_biniarizer.fit_transform(self.target2)

        x_train, x_test, y_train, y_test = train_test_split(
            self.data2, y_enc, test_size=0.33, random_state=RANDOM_STATE)

        print("labels shape:", y_train.shape)

        start = time.time()
        self.balanced_bagging.fit(x_train, y_train)
        end = time.time()
        print("Time spent is: ", (end - start))
        if self.num_labels > 1:
            print("Score: ", self.balanced_bagging.score(x_test, y_test))

        pred_test_std = self.balanced_bagging.predict(x_test)
        if self.num_labels > 1:
            print('\nPrediction accuracy:')
            print('{:.2%}\n'.format(metrics.accuracy_score(y_test, pred_test_std)))
        else:
            print(pred_test_std)
            print("test error:", pred_test_std[pred_test_std == -1].size)

        self.btn_clasificar.setDisabled(False)
        self.btn_clasificar_folder.setDisabled(False)
        print("Trained")

    def cb_degree_change(self, string):
        if self.cb_degree.currentText() != '':
            self.degree_selected = int(self.cb_degree.currentText())
        else:
            print(string)
            self.degree_selected = 1

    def classify_image(self, filename):
        path, only_file = os.path.split(os.path.abspath(filename))
        only_file_no_ext, only_file_extension = os.path.splitext(filename)

        image_cv2 = cv2.imread(filename)
        self.m_image = cv2.cvtColor(image_cv2, cv2.COLOR_BGR2GRAY)
        self.m_image_size = self.m_image.shape
        self.m_image_draw = cv2.cvtColor(self.m_image, cv2.COLOR_GRAY2BGR)

        height, width = self.m_image_draw.shape[:2]

        # self.m_image = imread(filename, as_gray=True)

        # print("x real: {}, y real: {}".format(self.m_image_size[0], self.m_image_size[1]))
        # with warnings.catch_warnings():
        #     warnings.simplefilter("ignore")
        #     self.m_image = img_as_ubyte(self.m_image)

        patch = self.sample_size
        self.coordinates_patches = []
        self.coordinates = []
        self.my_data = []
        self.list_save_pdf = []

        print("Altura imagen: {}, Anchura imagen: {}".format(height, width))

        scale_factor = 0.75
        self.m_image_draw2 = None
        self.m_image_draw3 = None
        if width > scale_factor * self.screen_width:
            print("Scale Width")
            self.m_image_draw2 = image_resize(self.m_image_draw, width=int(scale_factor * self.screen_width))

        if height > int(scale_factor * self.screen_height):
            print("Scale Height")
            if self.m_image_draw2 is not None:
                self.m_image_draw3 = image_resize(self.m_image_draw2, height=int(scale_factor * self.screen_height))
            else:
                self.m_image_draw3 = image_resize(self.m_image_draw, height=int(scale_factor * self.screen_height))

        windows_name = "samples"
        cv2.namedWindow(windows_name, cv2.WINDOW_NORMAL)
        cv2.moveWindow(windows_name, 550, 20)

        if self.m_image_draw2 is not None:
            height, width = self.m_image_draw2.shape[:2]
            cv2.resizeWindow(windows_name, width, height)
        if self.m_image_draw3 is not None:
            height, width = self.m_image_draw3.shape[:2]
            cv2.resizeWindow(windows_name, width, height)

        cv2.imshow(windows_name, self.m_image_draw)

        cv2.namedWindow(windows_name + '2', cv2.WINDOW_NORMAL)
        cv2.moveWindow(windows_name + '2', 50, 500)

        for pointx in range(int(math.ceil(patch / 2)), int(math.floor(self.m_image_size[1] + patch / 2)), patch):
            for pointy in range(int(math.ceil(patch / 2)), int(math.floor(self.m_image_size[0] + patch / 2)), patch):
                self.coordinates.append((pointx, pointy))
                self.coordinates_patches.append(self.m_image[int(pointy - patch / 2):int(pointy + patch / 2),
                                                int(pointx - patch / 2):int(pointx + patch / 2)])

                cv2.rectangle(self.m_image_draw, (int(pointx - patch / 2), int(pointy - patch / 2)),
                              (int(pointx + patch / 2), int(pointy + patch / 2)), (0, 255, 0), 3)
                cv2.imshow(windows_name, self.m_image_draw)
                cv2.imshow(windows_name + '2', self.coordinates_patches[-1])
                cv2.waitKey(100)

                features = extract_features(self.coordinates_patches[-1], int(self.halarick_distance))
                self.my_data = []
                self.my_data.append(
                    [features[0], features[1], features[2], features[3], features[4], features[5], features[6],
                     features[7], features[8], features[9], features[10], features[11], features[12], features[13]])

                data = np.array(self.my_data)
                test = data[0].reshape(1, -1)
                data2 = test

                self.predicted_value = self.balanced_bagging.predict(data2)
                if self.num_labels > 1:
                    np.set_printoptions(suppress=True, precision=3)
                    print("proba:", self.balanced_bagging.predict_proba(data2))
                    print("decision_function:", self.balanced_bagging.decision_function(data2))

                print('predicted value', self.multi_label_biniarizer.inverse_transform(self.predicted_value))
                out_names = list(
                    chain.from_iterable(self.multi_label_biniarizer.inverse_transform(self.predicted_value)))
                elements_without_no = [entry for entry in out_names if not (entry.endswith("_no"))]
                elements_with_no = [entry for entry in out_names if (entry.endswith("_no"))]

                # This lines should be uncommented to remove predictions where label and label_no appear
                if DELETE_REPEATED_LABELS:
                    for element in elements_with_no:
                        if element.startswith(tuple(elements_without_no)):
                            out_names.remove(element)
                            out_names.remove(element[:-3])
                    elements_without_no = [entry for entry in out_names if not (entry.endswith("_no"))]

                if len(elements_without_no) == 1:
                    if not (any(elements_without_no[0] + '_no' in s for s in out_names)):
                        print('DETECTED: {}'.format(elements_without_no[0]))
                        font = cv2.FONT_HERSHEY_SIMPLEX
                        cv2.putText(self.m_image_draw, elements_without_no[0],
                                    (int(pointx - 2 * patch / 5), int(pointy - 1 * patch / 5)), font, 1, (255, 255, 0),
                                    2, cv2.LINE_AA)
                        cv2.imshow(windows_name, self.m_image_draw)
                        self.list_save_pdf.append(
                            "Image: {0}, Point (x,y) = ({1},{2}), Size ={4}, DETECTED: {3}".format(
                                only_file, pointx, pointy, elements_without_no[0], self.sample_size
                            ))
                    else:
                        self.list_save_pdf.append(
                            "Image: {}, Point (x,y) = ({},{}), Size ={}".format(only_file, pointx, pointy,
                                                                                self.sample_size))
                else:
                    self.list_save_pdf.append(
                        "Image: {}, Point (x,y) = ({},{}), Size ={}".format(only_file, pointx, pointy,
                                                                            self.sample_size))
                    print("No label detected")
                    print(out_names)

        cv2.waitKey(0)
        cv2.destroyAllWindows()

        if self.debug_mode:
            pdf = PDF()
            pdf.set_margins(left=32, top=19, right=19)
            pdf.set_title('Report')
            pdf.set_author('Gustavo Plaza')
            pdf.print_chapter_list(1, filename, self.list_save_pdf)

            # pdf.print_chapter(2, 'THE PROS AND CONS', 'plot_gabor.py')

            im_pil = Image.fromarray(self.m_image_draw)
            image_data = io.BytesIO()
            im_pil.save(image_data, format='png')
            pdf.add_page()

            pdf.image('arbitrary_name_of_' + 'fsa.png',
                      w=150, h=150, x=int(A4_WIDTH / 2 - 150 / 2),
                      type='', link=None, file=image_data)
            pdf.output(only_file_no_ext + '.pdf', 'F')

        # self.btn_detalles.setDisabled(False)

    def on_input_file_button_clicked(self):
        filename, used_filter = QtWidgets.QFileDialog.getOpenFileName(parent=self, caption='Open file', dir='.',
                                                                      filter="Images (*.png *.jpg *.tif)")
        print(used_filter)
        if filename != '':
            self.classify_image(filename)
            self.btn_detalles.setDisabled(False)

    def load_svm(self):

        dial = QtWidgets.QMessageBox(self)
        dial.setText(u"From Where do you want to load the classifier")
        dial.setWindowTitle("Load Classifier")
        dial.setIcon(QtWidgets.QMessageBox.Question)
        load__file = dial.addButton('File', QtWidgets.QMessageBox.YesRole)
        load_ddbb = dial.addButton('Database', QtWidgets.QMessageBox.NoRole)
        dial.setDefaultButton(QtWidgets.QMessageBox.Close)
        dial.setEscapeButton(QtWidgets.QMessageBox.Close)
        close_btn = dial.addButton(QtWidgets.QMessageBox.Close)
        dial.exec_()

        if dial.clickedButton() == load__file:

            filename, used_filter = QtWidgets.QFileDialog.getOpenFileName(parent=self, caption='Open file', dir='.',
                                                                          filter='*.pkl')

            if filename != '':
                self.balanced_bagging = joblib.load(filename)
                self.btn_clasificar.setDisabled(False)
                self.btn_clasificar_folder.setDisabled(False)
                self.le_distance.setEnabled(True)

        elif dial.clickedButton() == load_ddbb:
            items = []
            db_pickles = self.Pickle_db.select()
            for pickle_db in db_pickles:
                items.append(pickle_db.pickle_name)

            (item, ok) = QInputDialog().getItem(self, self.tr("Classifier Name"),
                                                self.tr("Classifiers:"), items, 0, False)

            print(type(item))
            if ok and item:
                print(item)
                for pickle_db in db_pickles:
                    if pickle_db.pickle_name == item:
                        self.balanced_bagging = pickle.loads(pickle_db.pickle_value)
                        self.multi_label_biniarizer = pickle.loads(pickle_db.selector_value)

                        self.btn_clasificar.setDisabled(False)
                        self.btn_clasificar_folder.setDisabled(False)
        elif dial.clickedButton() == close_btn:
            print("Closed")

    def save_svm(self):

        dial = QtWidgets.QMessageBox(self)
        dial.setText(u"Where do you want to save the classifier")
        dial.setWindowTitle("Save Classifier")
        dial.setIcon(QtWidgets.QMessageBox.Question)
        save_file = dial.addButton('File', QtWidgets.QMessageBox.YesRole)
        save_ddbb = dial.addButton('Database', QtWidgets.QMessageBox.NoRole)
        dial.setDefaultButton(QtWidgets.QMessageBox.Close)
        dial.setEscapeButton(QtWidgets.QMessageBox.Close)
        close_btn = dial.addButton(QtWidgets.QMessageBox.Close)
        dial.exec_()

        btn = dial.clickedButton()
        if btn == save_file:

            file_name = QtWidgets.QFileDialog.getSaveFileName(self, 'Dialog Title', './',
                                                              selectedFilter='*.pkl')
            if file_name:
                print(file_name[0])

                joblib.dump(self.balanced_bagging, file_name[0])
        elif btn == save_ddbb:
            (text, ok) = QInputDialog.getText(self, self.tr("Save classifier"),
                                              self.tr("Classifier Name:"), QLineEdit.Normal,
                                              "Classifier " + "Name")
            if ok and text:
                print(text)

                pdata = pickle.dumps(self.balanced_bagging, pickle.HIGHEST_PROTOCOL)
                sdata = pickle.dumps(self.multi_label_biniarizer, pickle.HIGHEST_PROTOCOL)
                try:
                    self.pickle_db, self.pickle_db_created = self.Pickle_db.get_or_create(
                        pickle_value=pdata, pickle_name=text, selector_value=sdata)
                except peewee.IntegrityError:
                    print("Pickle already exists")
        elif btn == close_btn:
            print("Closed")


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    my_path = os.path.abspath(os.path.dirname(__file__))
    file_style = os.path.join(my_path, 'Minimalist_ok.qss')
    f = QFile(file_style)
    f.open(QFile.ReadOnly | QFile.Text)
    ts = QTextStream(f)
    stylesheet = ts.readAll()
    app.setStyleSheet(stylesheet)

    frame = MainWindow()
    frame.show()
    app.exec_()
