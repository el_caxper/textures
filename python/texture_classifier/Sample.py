from peewee import *
import datetime
import os
from playhouse.fields import PickleField

# actual_dir = os.path.dirname(os.path.abspath(__file__))
# db = SqliteDatabase(actual_dir+'/my_database.db')
db = SqliteDatabase(None)

class BaseModel(Model):
    class Meta:
        database = db

class Image(BaseModel):
    image_name = CharField(unique=True)

class Sample(BaseModel):
    image = ForeignKeyField(Image, backref='samples')
    label = CharField()
    x = IntegerField()
    y = IntegerField()
    size =  IntegerField()
    created_date = DateTimeField(default=datetime.datetime.now)
    positive = IntegerField()


class Feature(BaseModel):
    sample = ForeignKeyField(Sample, backref='features')
    label = CharField()
    ASM = FloatField()
    Contrast = FloatField()
    Correlation = FloatField()
    Sum_of_Squares_Variance =  FloatField()
    Inverse_Difference_Moment = FloatField()
    Sum_Average = FloatField()
    Sum_Variance = FloatField()
    Sum_Entropy = FloatField()
    Entropy = FloatField()
    Difference_Variance = FloatField()
    Difference_Entropy = FloatField()
    Information_Measure_of_Correlation_1 = FloatField()
    Information_Measure_of_Correlation_2 = FloatField()
    Maximal_Correlation_Coefficient = FloatField()


class Pickle(BaseModel):
    pickle_name = CharField(unique=True)
    pickle_value = PickleField(unique=True)
    selector_value = PickleField()

