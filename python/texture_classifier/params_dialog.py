import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtWidgets
from PySide2 import QtCore

from parameters_dialog import Ui_Parameters

class DialogWithCheckBox(QDialog,Ui_Parameters):

    def __init__(self, parent= None):
        super(DialogWithCheckBox, self).__init__(parent)

        self.setupUi(self)

        self.le_sample_size.setText('150')
        self.le_distance_sample.setText('5')
        self.chk_save_pdf.setChecked(True)
        self.chk_debug_mode.setChecked(False)
        self.chk_manual.setChecked(False)
        self.le_features_number.setText('all')
        self.le_features_number.setEnabled(False)
        self.chk_manual.stateChanged.connect(self.chk_manual_changed)

    def chk_manual_changed(self):
        print("New state: {}".format(self.chk_manual.isChecked()))
        if self.chk_manual.isChecked():
            self.le_features_number.setEnabled(True)
            self.le_features_number.setText("13")
        else:
            self.le_features_number.setEnabled(False)
            self.le_features_number.setText("all")

    def exec_(self, *args, **kwargs):
        """
        Override the exec_ method so you can return the value of the checkbox
        """
        return QDialog.exec_(self, *args, **kwargs), self.chk_debug_mode.isChecked(),\
               self.chk_save_pdf.isChecked(),self.le_distance_sample.text(),\
               self.le_sample_size.text(),self.chk_manual.isChecked(),self.le_features_number.text()
