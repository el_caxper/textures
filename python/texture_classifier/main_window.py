# -*- coding: utf-8 -*-

from PySide2 import QtCore, QtWidgets, QtWidgets
from PySide2.QtCore import QFile
from PySide2.QtCore import QTextStream

from PySide2.QtCore import QUrl
from PySide2.QtGui import QPixmap, QImage, QPainter

import pandas as pd
from os import listdir
from os.path import isfile, join
import os.path
import numpy as np

from sklearn import svm, metrics
from sklearn.externals import joblib


from Sample import Sample as Sampledb
from Sample import Image as Imagedb
from Sample import Feature as Featuredb

import peewee

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(564, 192)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setMaximumSize(QtCore.QSize(640, 435))
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(0, -1, 0, -1)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.verticalLayout_2.setContentsMargins(0, 0, -1, -1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.btn_entrenar = QtWidgets.QPushButton(self.centralwidget)
        self.btn_entrenar.setObjectName("btn_entrenar")
        self.verticalLayout_2.addWidget(self.btn_entrenar)
        self.btn_clasificar = QtWidgets.QPushButton(self.centralwidget)
        self.btn_clasificar.setObjectName("btn_clasificar")
        self.verticalLayout_2.addWidget(self.btn_clasificar)

        self.btn_clasificar_folder = QtWidgets.QPushButton(self.centralwidget)
        self.btn_clasificar_folder.setObjectName("btn_clasificar_folder")
        self.verticalLayout_2.addWidget(self.btn_clasificar_folder)

        self.btn_detalles = QtWidgets.QPushButton(self.centralwidget)
        self.btn_detalles.setObjectName("btn_detalles")
        self.verticalLayout_2.addWidget(self.btn_detalles)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        spacerItem = QtWidgets.QSpacerItem(30, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.cb_kernel = QtWidgets.QComboBox(self.centralwidget)
        self.cb_kernel.setObjectName("cb_kernel")
        self.gridLayout_3.addWidget(self.cb_kernel, 0, 1, 1, 1)
        self.lb_kernel = QtWidgets.QLabel(self.centralwidget)
        self.lb_kernel.setObjectName("lb_kernel")
        self.gridLayout_3.addWidget(self.lb_kernel, 0, 0, 1, 1)
        self.lb_degree = QtWidgets.QLabel(self.centralwidget)
        self.lb_degree.setObjectName("lb_degree")
        self.gridLayout_3.addWidget(self.lb_degree, 1, 0, 1, 1)
        self.cb_degree = QtWidgets.QComboBox(self.centralwidget)
        self.cb_degree.setObjectName("cb_degree")
        self.gridLayout_3.addWidget(self.cb_degree, 1, 1, 1, 1)
        self.lb_gamma = QtWidgets.QLabel(self.centralwidget)
        self.lb_gamma.setObjectName("lb_gamma")
        self.gridLayout_3.addWidget(self.lb_gamma, 2, 0, 1, 1)
        self.lb_C_param = QtWidgets.QLabel(self.centralwidget)
        self.lb_C_param.setObjectName("lb_C_param")
        self.gridLayout_3.addWidget(self.lb_C_param, 3, 0, 1, 1)
        self.le_gamma = QtWidgets.QLineEdit(self.centralwidget)
        self.le_gamma.setObjectName("lineEdit")
        self.gridLayout_3.addWidget(self.le_gamma, 2, 1, 1, 1)
        self.le_C = QtWidgets.QLineEdit(self.centralwidget)
        self.le_C.setObjectName("lineEdit")
        self.gridLayout_3.addWidget(self.le_C, 3, 1, 1, 1)

        self.btn_parameters = QtWidgets.QPushButton(self.centralwidget)
        self.btn_parameters.setObjectName("lb_distance")
        self.gridLayout_3.addWidget(self.btn_parameters, 4, 0, 1, 2)
        # self.le_distance = QtWidgets.QLineEdit(self.centralwidget)
        # self.le_distance.setObjectName("le_distance")
        # self.gridLayout_3.addWidget(self.le_distance, 4, 1, 1, 1)

        self.verticalLayout_3.addLayout(self.gridLayout_3)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 564, 23))
        self.menubar.setObjectName("menubar")
        self.menuArchivo = QtWidgets.QMenu(self.menubar)
        self.menuArchivo.setObjectName("menuArchivo")
        self.menuSVM = QtWidgets.QMenu(self.menuArchivo)
        self.menuSVM.setObjectName("menuSVM")
        self.menuAyuda = QtWidgets.QMenu(self.menubar)
        self.menuAyuda.setObjectName("menuAyuda")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionAbrir = QtWidgets.QAction(MainWindow)
        self.actionAbrir.setObjectName("actionAbrir")
        self.actionAcerca_de = QtWidgets.QAction(MainWindow, triggered=self.about)
        self.actionAcerca_de.setObjectName("actionAcerca_de")
        self.actionCargar_SVM = QtWidgets.QAction(MainWindow)
        self.actionCargar_SVM.setObjectName("actionCargar_SVM")
        self.actionGuardar_SVM = QtWidgets.QAction(MainWindow)
        self.actionGuardar_SVM.setObjectName("actionGuardar_SVM")
        self.menuSVM.addAction(self.actionCargar_SVM)
        self.menuSVM.addAction(self.actionGuardar_SVM)
        self.menuArchivo.addAction(self.actionAbrir)
        self.menuArchivo.addAction(self.menuSVM.menuAction())
        self.menuAyuda.addAction(self.actionAcerca_de)
        self.menubar.addAction(self.menuArchivo.menuAction())
        self.menubar.addAction(self.menuAyuda.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)



    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(
            QtWidgets.QApplication.translate("MainWindow", "Classifier SVM", None, -1))
        self.btn_entrenar.setText(
            QtWidgets.QApplication.translate("MainWindow", "Train the Classifier", None, -1))
        self.btn_clasificar.setText(
            QtWidgets.QApplication.translate("MainWindow", "Classification", None, -1))

        self.btn_clasificar_folder.setText(
            QtWidgets.QApplication.translate("MainWindow", "Folder Classification", None, -1))

        self.btn_detalles.setText(
            QtWidgets.QApplication.translate("MainWindow", "Details", None, -1))
        self.lb_kernel.setText(
            QtWidgets.QApplication.translate("MainWindow", "Kernel:", None, -1))
        self.lb_degree.setText(
            QtWidgets.QApplication.translate("MainWindow", "Degree:", None, -1))
        self.lb_gamma.setText(
            QtWidgets.QApplication.translate("MainWindow", "Gamma:", None, -1))
        self.lb_C_param.setText(
            QtWidgets.QApplication.translate("MainWindow", "C:", None, -1))
        self.btn_parameters.setText(
            QtWidgets.QApplication.translate("MainWindow", "Paramaters", None, -1))
        self.menuArchivo.setTitle(
            QtWidgets.QApplication.translate("MainWindow", "File", None, -1))
        self.menuSVM.setTitle(QtWidgets.QApplication.translate("MainWindow", "SVM", None, -1))
        self.menuAyuda.setTitle(
            QtWidgets.QApplication.translate("MainWindow", "Help", None, -1))
        self.actionAbrir.setText(
            QtWidgets.QApplication.translate("MainWindow", "Open Train Dir", None, -1))
        self.actionAcerca_de.setText(
            QtWidgets.QApplication.translate("MainWindow", "Abuot", None, -1))
        self.actionCargar_SVM.setText(
            QtWidgets.QApplication.translate("MainWindow", "Load SVM", None, -1))
        self.actionGuardar_SVM.setText(
            QtWidgets.QApplication.translate("MainWindow", "Save SVM", None, -1))

    def retranslateUiES(self, MainWindow):
        MainWindow.setWindowTitle(
            QtWidgets.QApplication.translate("MainWindow", "Clasificador SVM", None, -1))
        self.btn_entrenar.setText(
            QtWidgets.QApplication.translate("MainWindow", "Entrenar", None, -1))
        self.btn_clasificar.setText(
            QtWidgets.QApplication.translate("MainWindow", "Clasificar", None, -1))

        self.btn_clasificar_folder.setText(
            QtWidgets.QApplication.translate("MainWindow", "Clasificar Carpeta", None, -1))

        self.btn_detalles.setText(
            QtWidgets.QApplication.translate("MainWindow", "Ver detalles", None, -1))
        self.lb_kernel.setText(
            QtWidgets.QApplication.translate("MainWindow", "Kernel:", None, -1))
        self.lb_degree.setText(
            QtWidgets.QApplication.translate("MainWindow", "Grado:", None, -1))
        self.lb_gamma.setText(
            QtWidgets.QApplication.translate("MainWindow", "Gamma:", None, -1))
        self.lb_C_param.setText(
            QtWidgets.QApplication.translate("MainWindow", "C:", None, -1))
        self.btn_parameters.setText(
            QtWidgets.QApplication.translate("MainWindow", "Parametros:", None, -1))
        self.menuArchivo.setTitle(
            QtWidgets.QApplication.translate("MainWindow", "Menu", None, -1))
        self.menuSVM.setTitle(QtWidgets.QApplication.translate("MainWindow", "SVM", None, -1))
        self.menuAyuda.setTitle(
            QtWidgets.QApplication.translate("MainWindow", "Ayuda", None, -1))
        self.actionAbrir.setText(
            QtWidgets.QApplication.translate("MainWindow", "Abrir Train Dir", None, -1))
        self.actionAcerca_de.setText(
            QtWidgets.QApplication.translate("MainWindow", "Acerca de", None, -1))
        self.actionCargar_SVM.setText(
            QtWidgets.QApplication.translate("MainWindow", "Cargar SVM", None, -1))
        self.actionGuardar_SVM.setText(
            QtWidgets.QApplication.translate("MainWindow", "Guardar SVM", None, -1))
